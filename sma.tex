% -------------------------------------------------------------
% CAPÍTULO 3 - SISTEMAS MULTIAGENTE
% -------------------------------------------------------------

% -------------------------------------------------------------
\chapter{Sistemas multiagente}
\label{sma}
% -------------------------------------------------------------

Este capítulo se encarrega de descrever a técnica de sistemas multiagente, bem como suas características. Na \autoref{ia-agentes} é dissertada a relação entre inteligência artificial e agentes, além de considerar alguns aspectos importantes sobre inteligência artificial tradicional e distribuída. A seguir, na \autoref{sma-caracteristicas}, são explanados os conceitos fundamentais que circundam os sistemas multiagente e suas características. Nesta seção também são verificadas algumas relações que existem entre sistemas multiagente e sistemas distribuídos. Logo após, a \autoref{plataforma-jade} expõe um pouco da estrutura do \citeonline{jade}, o \textit{framework} utilizado para desenvolver os agentes do sistema proposto nessa monografia. Por fim, são realizadas algumas considerações a respeito dos tópicos abordados nesse capítulo.


% -------------------------------------------------------------
\section{Inteligência artificial e agentes}
\label{ia-agentes}
% -------------------------------------------------------------

A computação começou a ganhar corpo de ciência após o fim da Segunda Guerra Mundial e, a partir desse momento, muitas de suas subáreas passaram a ser desenvolvidas por pessoas que apostavam na ciência como revolucionária das próximas décadas --- e assim o foi. Sistemas Operacionais, linguagens de programação, redes de computadores e outras vertentes ganharam grande atenção dos pesquisadores na época.

Uma das áreas que também emergiu nesse contexto foi a inteligência artificial (IA), que na época era vista como uma maneira de os computadores interagirem com humanos de uma forma que fosse mais natural. Desde esses tempos uma pergunta intriga grandes nomes da ciência da computação: \textit{``o que é inteligência artificial?''}. Para respondê-la, é necessário desbravar áreas além da computação em busca da raiz do termo \textit{inteligência}, envolvendo ciências como a filosofia, as ciências sociais, a psicologia, entre outras \cite{saraiva}. Mesmo após discussões conjuntas entre as áreas, ainda não se tem resposta universal para o que é \textit{inteligência}, dessa forma, tampouco haveria consenso para o termo \textit{inteligência artificial}. Ainda assim, muitos autores se lançam a definir o termo, contudo os mesmos reconhecem que não são definições universais.

Há diversos pontos de vista que norteiam essas definições --- segundo \citeonline{russel}, há quatro correntes de pensamento ---, mas suas discussões estão fora do escopo deste trabalho. Para esta pesquisa, a definição de \citeonline{rich} será suficiente, que diz que inteligência artificial é o estudo de como programar os computadores de forma que façam atividades que, até o momento, as pessoas fazem melhor. Assim, desvia-se o foco do ``computador inteligente'' e concentra-se na ideia de ``ação inteligente''. Entende-se por ação inteligente a realização de atividades que exijam certo grau de inteligência dos seres humanos. É possível programar computadores para realizarem ações inteligentes e, dentro dessa questão, aparecem várias técnicas para se conseguir realizar esse tipo de programação.

Uma forma de programar ações inteligentes é através do conceito de \textit{agentes inteligentes}. Na área da inteligência artificial, define-se agente inteligente como uma entidade que pode perceber o ambiente através da sua leitura por meio de \textit{sensores} e consegue interagir sobre ele por meio de \textit{atuadores} \cite{russel}. Aqui entende-se ``entidade'' como qualquer objeto físico ou não, isto é, ultrapassa os conceitos gerais da robótica e computação. De uma forma mais específica, \citeonline{wooldridge} define um agente como um sistema computacional imerso em um ambiente e que realiza ações \textit{autônomas} sobre ele para alcançar os objetivos que lhes foram determinados. Em suma, um agente é um sistema autônomo que realiza ações inteligentes através da percepção e atuação sobre o ambiente no qual está inserido. O termo \textit{agente autônomo} significa que ele consegue realizar suas ações sem a intervenção direta de comandos externos ou controle sobre seu estado atual \cite{bellifemine}. Para realizar a leitura e a interação com o ambiente, um agente precisa de dois componentes fundamentais denominados sensores e atuadores \cite{russel}.

\begin{itemize}
	\item \textit{Sensores}: são meios pelos quais o agente terá acesso à informações de estado do ambiente onde ele está inserido. Na computação, em geral, esses sensores são dispositivos e/ou softwares que leem o estado do ambiente e mantém esses dados em memórias;
	\item \textit{Atuadores}: são meios pelos quais o agente poderá agir sobre o ambiente. Em geral, essa ação se dá pelo resultado do processamento dos dados de entrada à luz dos objetivos que o agente tem. Na computação, esses atuadores podem ser dispositivos de saída (como monitores, \textit{leds} ou caixas de som), atuadores mecânicos (como braços ou pernas robóticas), entre outros.
\end{itemize}

Fazendo uma analogia, podemos considerar um pequeno robô autônomo com um agente inteligente. Seus ``olhos'' seriam seus sensores e seus braços e pernas mecânicas seus atuadores. A \autoref{fig-agent-scheme} ilustra essa analogia. Como alternativa, um software pode ser considerado um agente se consegue operar autonomamente em um ambiente e, sem comandos externos, observá-lo --- através de uma variável ou configuração, por exemplo --- e atuar sobre ele --- escrevendo um arquivo ou fazendo \textit{prints} na tela, por exemplo.

% Figura com modelo esquemático de um agente
\begin{figure}[htb]
	\caption{\label{fig-agent-scheme}Modelo esquemático de um agente}
	\begin{center}
		\includegraphics[scale=0.4]{figures/fig-agent-scheme.pdf}
	\end{center}
	\legend{Fonte: Adaptado de \citeonline[p. 49]{saraiva}}
\end{figure}

As atividades que os agentes realizam durante o seu ciclo de vida são descritas como comportamentos. Comportamentos, então, podem ser definidos em termos do que o agente precisa executar, colocando cláusulas para que determinadas tarefas sejam realizadas ou não. Os comportamentos podem descrever três faces, em particular, de cada objetivo do agente: \textit{o que fazer}, \textit{quando fazer} e \textit{como fazer}. Cabe delinear que, dependendo da abordagem utilizada para a programação dos agentes, essas faces dos comportamentos podem ser alteradas em função de como o agente percebe o ambiente, podendo até mesmo definir novos objetivos durante o seu ciclo de vida. Isso faz parte da característica autônoma que os agentes inteligentes possuem, isto é, devem se adaptar por si só ao ambiente no qual estão imersos. Para diversos autores, todo agente exibe algumas características fundamentais em seus comportamentos que estão atreladas à sua existência e operação. Entre uma variação ou outra, os itens abaixo listam as principais características que um agente inteligente possui \cite{saraiva};\cite{bellifemine};\cite{wooldridge};\cite{weiss}.

\begin{itemize}
	\item \textit{Reatividade}: é a habilidade de perceber o ambiente e suas variações e responder esses a estímulos em um tempo aceitável, de forma que a base de dados do agente tenha dados atuais sobre o estado do ambiente;
	\item \textit{Proatividade}: ser proativo significa que o agente é capaz de tomar as iniciativas para agir sobre o ambiente, executando seus comportamentos para alcançar seus objetivos;
	\item \textit{Habilidade social}: diz respeito à capacidade de interagir com outros agentes (humanos ou não) para satisfazer seus objetivos;
\end{itemize}

A capacidade de um agente agir socialmente está relacionada com a sua capacidade de comunicação. Um agente que consegue se comunicar, acaba por trocar ``experiências'' com a entidade a qual se comunica. Essa comunicação se dá pela troca coordenada de mensagens, e pode ser feita entre dois ou mais agentes ou outra entidade qualquer que entenda a linguagem utilizada pelo agente na sua comunicação. Assim, se estabelece a necessidade de haver uma padronização a respeito da linguagem usada entre agentes e sistemas que pretendem manter uma comunicação ao longo do tempo. Em termos de computação, essa comunicação se dá através de redes de computadores, utilizando os protocolos de comunicação vigentes e herdando todas as suas características. Mais características a respeito da comunicação entre agentes serão dadas na \autoref{sma-caracteristicas}, pois a partir desse ponto há o estabelecimento de uma sociedade de agentes, denominada \textit{sistema multiagente}.

À um primeiro olhar, agentes possuem uma raiz muito próxima à ideia de objetos do paradigma de programação orientada a objetos. Em ambos, objetos e agentes são independentes uns dos outros, executam tarefas definidas e se comunicam através da troca coordenada de mensagens. Essas características ajudaram o desenvolvimento dos sistemas multiagente ao longo dos anos, já que acompanharam, em certo aspecto, o desenvolvimento das linguagens orientadas a objetos \cite{bellifemine}. Além disso, as linguagens de orientação a objetos (OO) possuem características que favorecem a implementação de agentes, tão verdade que muitos \textit{frameworks} para programação multiagentes atuais se utilizam de linguagens e bibliotecas OO em suas implementações, tais quais o \citeonline{jade}, por exemplo.

Entretanto, há que se pontuar que técnicas tão parecidas possuem diferenças chave que lhes atribuem o caráter de serem únicas. As principais diferenças entre essas duas modalidades de programação se dão em três pontos, a saber autonomia, flexibilidade e controle de processo \cite{wooldridge}\cite{weiss}; \cite{bellifemine}.

\textit{Autonomia}, como já definido, dá ao agente a possibilidade de executar seus comportamentos por iniciativa própria, não necessitando que um agente externo lhe dê estímulo para que isso ocorra. Na programação OO, um objeto só executa seu comportamento (método) se for invocado por algum outro objeto em algum momento da execução.

A \textit{flexibilidade} está relacionada ao tipo de comportamento que um agente executa, isto é, há uma escolha ativa do agente sobre a decisão de executar ou não determinado comportamento e, em caso positivo, que tipo de comportamento deve ser  executado (reativo, proativo ou social). Em relação aos objetos, qualquer chamada a seus métodos deve ser feita de maneira explícita, indicando qual método será executado, mesmo que estes tenham a vantagem do polimorfismo. Além do mais, objetos não podem escolher se executam ou não determinado método, mas necessariamente respondem à todas as chamadas que lhes são feitas.

Finalmente, em termos de \textit{controle de processo}, um agente tem total controle sobre os processos e \textit{threads} que ele executa naquele momento, podendo criar, parar ou remover \textit{threads} que estejam executando seus comportamentos. No caso da estrutura OO, não há controle por parte do objeto sobre as \textit{threads} que executam suas atividades, pelo contrário, por questões de segurança, abstraem essa tarefa de si.

A técnica de desenvolver agentes utiliza a ideia do aprendizado coletivo. Isso significa que um agente pode aprender por meio da interação com o ambiente (aprendizado indireto) e também pela interação com outros agentes (aprendizado direto) \cite{bellifemine}. Essa ideia básica traz consequências relevantes para a programação com agentes, pois entra num ramo mais recente da IA, a inteligência artificial distribuída.

Segundo \citeonline{saraiva}, a inteligência artificial distribuída é a parte da IA que estuda as relações de aprendizado por interação entre múltiplos agentes inteligentes. Para o autor, cada agente possui uma visão específica do ambiente em que está inserido e, por meio das interações entre outros agentes, atinge um determinado objetivo. Essa parte da IA se difere da IA tradicional porque esta é vista como um único sistema inteligente que é capaz de simular aprendizado e tomada de decisão. A IA distribuída, por outro lado, concentra-se no fato do conhecimento estar distribuído, simulando os comportamentos inteligentes através do compartilhamento das múltiplas visões do estado do sistema. A vantagem está no fato de ser possível simular com mais precisão ambientes que por natureza são distribuídos, tais quais os \textit{smart grids}.

\citeonline{bellifemine} complementam essa ideia trazendo afirmações a respeito da eficácia desse tipo de método para resolver problemas complexos, já que haveria um processamento descentralizado. Como resultado de tal distinção, novas técnicas surgem para explorar a vertente de aprendizado e inteligência artificial distribuída, como a técnica de sistemas multiagente.


% -------------------------------------------------------------
\section{Sistemas multiagente e suas características}
\label{sma-caracteristicas}
% -------------------------------------------------------------

O processo de aprendizagem na IA distribuída é diferente dos processos de aprendizagem da IA tradicional, conforme discutido na seção anterior. Sabe-se que a interação é um fator imprescindível para técnicas da IA distribuída, portanto, as redes de comunicação são requisito básico para esse tipo de abordagem. No âmbito dos agentes, o processo de aprendizagem se dá, da mesma forma, por meio da comunicação entre vários agentes, podendo ser agente humanos ou não \cite{saraiva}. À reunião de vários agentes inteligentes, imersos em um ambiente, conectados por uma rede de comunicação e se coordenando pela troca de mensagens, dá-se o nome de \textit{sistema multiagente} (SMA).

\citeonline{saraiva}, por sua vez, define sistema multiagente como um conjunto de agentes em um ambiente computacional e que implementam as técnicas de inteligência artificial distribuída. Um sistema multiagente, portanto, é um plataforma \textit{peer-to-peer}, já que agentes autônomos são quem requisitam e proveem toda a informação que circula no sistema. Com o termo \textit{autônomo} diz-se que os agentes não precisam ter, necessariamente, um ponto central para fazer requisições ou se reportar, pois todos podem ser, ao mesmo tempo, provedores e requisitantes dos recursos de informação do sistema, eliminando assim, nativamente, a figura do ponto central de falhas.

É interessante notar que a definição de SMA é muito próxima à definição de sistemas distribuídos. \citeonline{coulouris} definem um sistema distribuído como diferentes componentes localizados em computadores interligados por uma rede e se comunicam e coordenam através da troca de mensagens. Assim, pode-se classificar SMA como um tipo de sistema distribuído, com o adendo que o SMA trabalha autonomamente devido as características dos agentes que o compõe. Dessarte, SMA são muito vantajosos quando trabalham com problemas distribuídos e sobre sistemas distribuídos \cite{saraiva}. Esse se torna então o principal motivo por optar pelo uso da técnica de SMA nesta pesquisa, uma vez que o problema de autorrecuperação nos \textit{smart grids} é também um problema distribuído.

Um ator muito importante no funcionamento dos SMA é a mensagem, pois é o objeto que carregará as informações entre os agentes que se comunicam. Como consequência, as mensagens precisam seguir um protocolo bem definido, como fim de que os múltiplos agentes consigam compreender as informações que são trocadas no sistema e para proporcionar até mesmo a interoperabilidade entre diferentes sistemas multiagente. Existem alguns esforços atuais no sentido de prover especificações para essas linguagens e uma delas é a \citeonline{fipa}, que propõe um protocolo muito utilizado pelos diversos \textit{framework} para multiagentes \cite{bellifemine}.

Evidencia-se, porém, que um protocolo de comunicação de agentes não obriga a implementação de um protocolo de redes específico, ficando a cargo do desenvolvedor escolher qual o protocolo mais adequado para sua abordagem. De toda forma, toda mensagem em sistemas multiagentes deve expressar --- além de especificar campos derivados dos atuais protocolos de rede, tais como endereço IP e MAC do remetente e destinatário, porta, entre outras coisas --- pelo menos dois campos: ato comunicativo e conteúdo.

\textit{Ato comunicativo} é a informação que definirá qual o tipo de comunicação que será feita entre os agentes. Fazendo uma analogia, quando duas pessoas conversam por texto na língua portuguesa, uma frase é definida como uma pergunta quando termina com o caractere ``\textit{?}''. Isso significa que haverá o aguardo por uma resposta, salvo alguns casos, pelo emissor da mensagem. De outra maneira, uma frase que termina com o caractere ``\textit{.}'' pode significar uma resposta ou uma simples informação. O segundo campo, o \textit{conteúdo}, diz repeito ao que será transmitido na comunicação. Em termos de protocolo de comunicação dos agentes, uma padronização nessa forma de trocar mensagens pode significar a interoperabilidade entre os diversos e heterogêneos SMA que possam existir.

% -------------------------------------------------------------
\subsection{Arquiteturas de sistemas multiagente}
\label{sma-arquiteturas}
% -------------------------------------------------------------

A possibilidade de interação entre múltiplos agentes do sistema abre precedentes para as mais diversas formas de organização dos agentes. \citeonline{bellifemine} definem essas formas de organizar os agentes como arquiteturas de SMA e, segundo os autores, podem ser classificadas em quatro tipos:

\begin{itemize}
	\item \textit{Arquitetura baseada em lógica}: é um tipo de arquitetura baseada em linguagem simbólica, como a dos seres humanos. Tem raízes na lógica simbólica e, por isso, possui uma lógica fácil de entender, porém, tem problemas quanto ao tempo de processamento das respostas e à abstração do mundo real, que nem sempre se dá por símbolos;
	\item \textit{Arquitetura reativa}: é baseada na dualidade estímulo-resposta. Os agentes atuam sobre um ambiente em função do que eles conseguem ler do mesmo ou dos estímulos que recebem de outros agentes. É vantajoso por produzir respostas rápidas em ambientes dinâmicos e são facilmente adaptáveis. Porém, tomam as decisões somente baseados na leitura do ambiente, que nem sempre é correta ou clara. Além disso, sistemas reativos não conseguem aprender por experiência;
	\item \textit{BDI}: baseia-se na formação de regras através de lógica modal que descrevem os ``desejos'' do agente. Possui quatro estruturas de dados básicas na sua construção: \textit{belief}, que representa as informações que um agente tem sobre um ambiente; \textit{desire}, que representa os objetivos que o agente tem, baseado nos dados lidos do ambiente (\textit{belief}); \textit{intention}, que são os comportamentos que o agente deverá executar (subconjunto de \textit{desire}); e \textit{plan}, que é uma estrutura linear com os passos que o agente deverá executar para concluir suas tarefas (\textit{intention}). Nesses casos, dependendo de como foi programado, um agente consegue criar novos objetivos à medida que convive com outros ou observa o ambiente. É ideal para técnicas de aprendizado, porém, possui uma linguagem não favorável à adaptação OO.
	\item \textit{Arquitetura em camadas}: utiliza camadas que funcionam como mini agentes. Essas arquiteturas podem ser organizadas em camadas horizontais ou verticais. Na arquitetura \textit{horizontal} as camadas são conectadas diretamente ao sensores e atuadores do agente, trabalhando mutuamente para executar as ações baseado-se na leitura dos sensores. Já nas arquiteturas verticais as camadas possuem uma hierarquia. Só uma camada é conectada diretamente nos sensores e outra nos atuadores, obrigando o fluxo de informação passar pelas camadas intermediárias e mediando conflitos entre elas.
\end{itemize}

Há autores que também classificam arquiteturas de SMA dependendo de como os agentes interagem. Para isso, esses autores se baseiam nas relações sociais humanas e replicam esses conceitos em SMA. Assim, tem-se sistemas onde os agentes agem com concorrência, cooperação ou negociação, formando modelos como sociedades, hierarquia, mercado, federação, entre outros.

Os SMA atualmente são muito utilizados para resolver problemas complexos e simular ambientes para prever fatos. Sistemas como simulação de partículas, previsão climática \cite{lee}, simulação do comportamento das bolsas de valores \cite{luo}, pesquisas simuladas de implantação de mercado \cite{praca}, sistemas de segurança, sistemas de distribuição hídrica, sistemas elétricos de potência \cite{eddy}, problemas de otimização são alguns exemplos dos campos onde os SMA são usados como meio de se conseguir obter soluções próximas daquilo que se espera.


% -------------------------------------------------------------
\section{A plataforma JADE}
\label{plataforma-jade}
% -------------------------------------------------------------

Existe uma série de plataformas para desenvolvimento de sistemas multiagente, cada uma com foco em diferentes arquiteturas com o fim de incentivar o estilo \textit{Agent-Oriented Programing} \cite{bellifemine} de desenvolver \textit{software}. Uma plataforma muito usada é o \citeonline{jade}, que segundo os seus criadores, é um \textit{middleware} que traz facilitadores para a implementação de SMA. O JADE é baseado na linguagem de programação Java \cite{oracle} e por isso suporta uma série de bibliotecas que acompanham a linguagem, além de prover algumas ferramentas para análise de ambientes, checagem de estados de agentes entre outras.

No JADE, os agentes são organizados em plataformas e contêineres. \textit{Plataformas} são os espaços mais básicos num ambiente JADE e são elas que abrigarão todos os contêineres e agentes do sistema. É importante ressaltar que uma plataforma identifica um conjunto de agentes, o que significa que o nome da plataforma compõe os endereços dos agentes que nela residem. Agentes de uma mesma plataforma também compartilham recursos e se comunicam mais facilmente, porém esse fato não impede que plataformas diferentes consigam se comunicar, desde que se saiba o endereço e o protocolo de comunicação utilizado na plataforma com a qual se deseja manter comunicação. \textit{Contêineres} são subconjuntos das plataformas que abrigam de fato os agentes do sistema. Diferentes contêineres podem pertencer a uma mesma plataforma, além de poderem ser executados em diferentes \textit{hosts}, entretanto, todo contêiner deve estar registrado em uma única plataforma.

% Figura com esquema organizacional do JADE
\begin{figure}[htb]
	\caption{\label{fig-jade-organization}Esquema organizacional do JADE}
	\begin{center}
		\includegraphics[scale=0.5]{figures/fig-jade-organization.pdf}
	\end{center}
	\legend{Fonte: Adaptado de \citeonline[p. 6]{caire}}
\end{figure}

Toda plataforma tem um contêiner principal, que é de onde dois agentes específicos (AMS e DF) vão exercer o papel de gerência da plataforma e dos recursos compartilhados entre os agentes. Esses agentes proveem espaços para a publicação de serviços (páginas amarelas), reconhecimento de agentes (páginas brancas) e outros recursos compartilhados que visam garantir a comunicação entre os diferentes agentes do sistema. Essa estrutura básica de todo SMA programado em JADE está ilustrada na \autoref{fig-jade-organization}.

Todo agente em JADE possui um ciclo de vida bem definido, que é controlado pelo próprio agente, dependendo do alcance de seus objetivos, onde é possível observar três momentos típicos. A \textit{inicialização} é o momento onde o agente receberá todas as informações das quais precisará durante o seu ciclo de vida, além de adicionar seus comportamentos iniciais. O momento de \textit{execução} é quando o agente está operando e executando seus comportamentos de acordo com a variação do ambiente e os estímulos que recebe. Por fim, o momento de \textit{finalização} ocorre quando o agente atingiu seu objetivo final e executa as suas últimas operações antes de ser finalizado e encerrar todos os seus processos. A \autoref{fig-agent-life-cycle} apresenta um fluxograma que resume o ciclo de vida de um agente no JADE, onde os comandos destacados em vermelho são métodos dos agentes e comportamentos executados pelo sistema.

% Figura com fluxograma do ciclo de vida dos agentes no JADE
\begin{figure}[htb]
	\caption{\label{fig-agent-life-cycle}Fluxograma do ciclo de vida dos agentes no JADE}
	\begin{center}
		\includegraphics[scale=0.8]{figures/fig-agent-life-cycle.pdf}
	\end{center}
	\legend{Fonte: Adaptado de \citeonline[p. 11]{caire}}
\end{figure}

Um agente pode executar uma série de comportamentos durante o seu ciclo de vida. Os comportamentos em JADE são classes Java que definem as tarefas a serem executadas em cada comportamento e podem ser de dois tipos básicos: comportamentos com fim definido ou comportamentos com fim indefinido. Como o nome já explica, \textit{comportamentos com fim definido} são comportamentos que executam suas tarefas --- ou não --- e depois são finalizado. Existem muitas variações desse tipo de comportamento, mas foge do escopo deste trabalho uma discussão detalhada sobre eles. Da mesma forma, \textit{comportamentos com fim indefinido} são comportamentos cíclicos e não finalizam até que o agente seja finalizado. Também existem uma série de variações dos comportamentos cíclicos e são usadas de acordo com as tarefas que precisam realizar. Vale frisar também que essas variações de comportamentos podem ser temporais, compostas, simuladoras de máquinas de estado, entre outras.

O JADE também oferece ferramentas para a comunicação entre agentes. Como todo \textit{framework} para sistema multiagente, o JADE pré-implementa uma biblioteca que lida com toda a complexidade da comunicação, sendo necessário apenas informar dados sobre o \textit{receptor}, \textit{ato comunicativo} e \textit{conteúdo} da mensagem. A passagem de mensagens no JADE é assíncrona \cite{caire}, o que traz vantagens como o não-bloqueio dos agentes que enviam ou recebem as mensagens. Isso permite que os agentes continuem a executar suas tarefas sem ficarem presos à espera por operações de rede e de arquivos. A principal desvantagem em sistemas distribuídos desse tipo é a dificuldade em saber se haverá resposta daquela requisição, precisando estabelecer técnicas de \textit{timeout} para escapar de possíveis falhas \cite{coulouris}. Esse problema, logicamente, se estende para SMA, porém os agentes têm a flexibilidade de tomar diversas decisões em casos como esse, podendo, por exemplo, contornar o problema verificando com outros agentes próximos se podem atender à sua requisição.

Todo o processo de troca de mensagem é controlado pelo JADE, que entrega as mensagens à pilha que cada agente possui. Ao receber uma mensagem, o agente pode estabelecer uma série de filtros que lhe permite decidir o que fazer sobre uma mensagem e o seu conteúdo e o momento de fazê-lo. A troca de mensagens no JADE utiliza a linguagem ACL e protocolo MTP (\textit{Message Trasnport Protocol}), ambos seguindo as determinações da \citeonline{fipa}. O protocolo MPT não obriga o programador a utilizar um protocolo da camada de aplicação específico, mas possui pré-configurado, por questões de convenção, o protocolo HTTP para transmitir as mensagens. A \autoref{fig-jade-communication} retrata resumidamente como é feita a troca de mensagens no JADE.

% Figura com esquema de comunicação dos agentes no JADE
\begin{figure}[htb]
	\caption{\label{fig-jade-communication}Esquema de comunicação dos agentes no JADE}
	\begin{center}
		\includegraphics[scale=0.85]{figures/fig-jade-communication.pdf}
	\end{center}
	\legend{Fonte: Adaptado de \citeonline[p. 15]{caire}}
\end{figure}


% -------------------------------------------------------------
\section{Considerações do capítulo}
% -------------------------------------------------------------

O presente capítulo definiu termos relativos à inteligência artificial e agentes inteligentes, que são primordiais para a fundamentação teórica de sistemas multiagente. Não há um consenso que defina se agentes são realmente inteligentes, mas sabe-se que podem simular ações inteligentes. Um agente precisa interagir com o ambiente e com outros agentes, processos pelo qual ele aprende. Um sistema multiagente é a coleção de diversos agentes inteligentes que interagem uns com os outros com o fim de alcançarem seus objetivos e podem ser organizados de diversas maneiras. Cada tipo de arquitetura de SMA é adequado para um ou outro problema dependendo da abordagem utilizada. A ferramenta utilizada para implementar o SMA proposto neste trabalho é o JADE, \textit{framework} baseado na linguagem Java e que possui estruturas de organização e comunicação distribuídas, característica intrínseca dos SMA.

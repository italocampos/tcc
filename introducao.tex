% ----------------------------------------------------------
% CAPÍTULO 1 - INTRODUÇÃO
% ----------------------------------------------------------

\chapter{Introdução}\label{introducao}

%\chapterprecis{Isto é uma sinopse de capítulo. A ABNT não traz nenhuma
%normatização a respeito desse tipo de resumo, que é mais comum em romances 
%e livros técnicos.}\index{sinopse de capítulo}

Este capítulo descreve brevemente o contexto do trabalho, sua relevância, os trabalhos relacionados, a metodologia, o aparato tecnológico utilizado e outras características que resumem a organização do texto.

% -------------------------------------------------------------
\section{O Sistema Elétrico de Potência e suas características}
% -------------------------------------------------------------

A energia elétrica é uma das formas de energia mais importantes para o desenvolvimento da sociedade atual. Com ela, é possível realizar desde as atividades mais básicas da economia até fabricar a mais nova tecnologia a ser utilizada na indústria. Por essas e outras razões, uma demanda crescente pelo consumo de energia vem se instaurando na sociedade ao redor do mundo, o que confere aos sistemas elétricos grande complexidade e requer destes o desdobramento necessário para atender às questões de geração e distribuição de energia elétrica \cite{gebran}. Assim, torna-se cada vez mais necessário o uso de técnicas sofisticadas para que o sistema elétrico consiga dar conta das demandas que são a ele submetidas. Nesse sentido, é interessante que novas tecnologias forneçam maneiras de se produzir e distribuir energia elétrica de forma otimizada \cite{gebran} e que atenda à maior parte da demanda dentro de limites de qualidade estabelecido pelos órgãos competentes.

Quando se fala em geração e transmissão de energia elétrica em termos de Brasil, refere-se em grande parte ao parque hidrelétrico nacional, que figura entre os maiores do mundo em produção de energia elétrica \cite{mauad-ferreira-trindade}. Segundo dados da Agência Nacional de Energia Elétrica, a ANEEL, a produção de energia no Brasil por hidrelétricas ainda é a que corresponde à maior cota na matriz energética do país, chegando aos 60,36\% \cite{aneel-big} de toda a potência produzida no país. Seguindo as hidrelétricas, estão as usinas termoelétricas e campos eólicos, com 25,54\% e 8,35\%, respectivamente \cite{aneel-big}. Considerando esse cenário, há a necessidade de se gerenciar todo o processo de geração e transmissão de energia elétrica dos pontos onde ela é gerada até o usuário final.

Um sistema elétrico de potência é basicamente dividido em três subsistemas, compreendidos em \textbf{geração}, \textbf{transmissão} e \textbf{distribuição} de energia elétrica \cite{sarma}, cada um com características e complexidades próprias. Este trabalho tem foco nos sistemas de distribuição, que, em suma, são os responsáveis por transportar energia elétrica de baixa tensão aos consumidores finais. Neste contexto, aparecem uma modalidade de sistemas elétricos chamados \textit{smart grids}, que são redes inteligentes capazes de realizar automaticamente algumas funções, entre elas a de autorrecuperação. A autorrecuperação consiste basicamente na recuperação automática de uma rede que apresentou alguma falha e, em decorrência disso, interrompeu o fornecimento de energia em um conjunto de consumidores do sistema.

Segundo dados da \citeonline{aneel-icc}, em 2017, em média, cada unidade consumidora no Brasil contou com aproximadamente 14 horas de interrupção, distribuídas em aproximadas 8  interrupções computadas pelo órgão \cite{aneel-icc}. Isso significa uma média de 1 hora e 45 minutos de falta de energia por cada interrupção no fornecimento. Analisando os últimos cinco anos, há uma média de 17 horas de interrupção por ano, com aproximadas 9 interrupções computadas no fornecimento de energia elétrica \cite{aneel-icc}. A recuperação de falhas nas redes elétricas do Brasil atualmente é feita majoritariamente de maneira manual, explicando a quantidade de tempo que decorre de cada falha no sistema elétrico de distribuição. Investiga-se aqui qual será o impacto da aplicação de um sistema que se recupere automaticamente, utilizando um sistema multiagente para tal feito.

Este não é um problema trivial a se resolver, uma vez que é necessário considerar múltiplos cenários em um ambiente altamente dinâmico e com muitas variáveis a serem consideradas. \citeonline{pramio} destaca o problema de autorrecuperação como um problema de otimização combinatória, sendo inviável em custo computacional encontrar uma solução ótima. Para atacar esse tipo de problema, muitos autores se valem de técnicas da computação para se obter resultados próximos aos da solução ótima com um custo computacional viável. Conforme \citeonline{saraiva}, uma das técnicas utilizadas para tal fim é a de sistemas multiagente, que consiste em modelar um sistema por meio de agentes independentes que se coordenam mutuamente para alcançar um objetivo comum \cite{reis}. Ainda segundo \citeonline{saraiva}, utilizar sistemas multiagente para lidar com problemas do sistema de potência é vantajoso, já que o sistema elétrico possui características que favorecem a utilização dessa técnica.


% -------------------------------------------------------------
\section{Trabalhos relacionados}
% -------------------------------------------------------------

O tema \textit{autorrecuperação} ganhou foco entre  a comunidade de pesquisadores de \textit{smart grids} ao longo dos últimos anos, rendendo uma quantidade considerável de trabalhos sobre o tema atualmente. É possível observar que diferentes abordagens vêm sendo utilizadas para alcançar a autorrecuperação, levando em conta fatores importantes como desempenho dos métodos, robustez, tolerância a falhas, dinamismo, entre outros. A seguir, são brevemente relatadas e discutidas algumas dessas abordagens, com foco no método utilizado por cada uma.

Iniciando pelo trabalho de \citeonline{souza}, verifica-se a utilização de SMA para lidar com o problema da autorrecuperação. No trabalho, o autor se utiliza de um modelo de autorrecuperação já publicado por outro autor, que sugere uma autorrecuperação em três etapas e utilizando energia provinda de fontes de geração distribuídas. \citeonline{souza} propõe uma quarta etapa, que é o restabelecimento de energia por um processo de ilhamento e corte arbitrário de cargas, com base na sua prioridade. Cargas com maior prioridade, em termos práticos, são locais nos quais a interrupção de energia elétrica pode significar consequências de alto impacto, como hospitais, prédios de serviços públicos básicos, entre outros. A abordagem do autor considera que nos casos de falta, mesmo que não seja possível recuperar todas as cargas do sistema, poderá ser empregado o ilhamento intencional de cargas através do corte de algumas com baixa prioridade, com o fim de restabelecer o fluxo energético pelo menos nos pontos com maiores prioridades da rede. O trabalho modela e propõe um sistema multiagente reativo para detectar e isolar falhas na rede, além de realizar negociações e operações de chaveamento. A abordagem do autor tem maior sucesso nos casos onde a energia provinda de fontes distribuídas é suficiente para dar conta das mínimas demandas das ilhas. Observa-se nesse trabalho que o caráter distribuído do SEP é acompanhado adequadamente por sistemas multiagente.

\citeonline{wang} trazem um modelo mais abstrato para a autorrecuperação. Os autores utilizam o conceito de \textit{microgrids} independentes que mantém informações, controle e as restrições de todas as cargas que lhe são submetidas. O sistema proposto é dividido em camadas de comunicação, onde as camadas mais altas se comunicam independentemente e não têm informações sobre as cargas que pertencem à outras \textit{microgrids}. As camadas mais baixas dizem respeito à comunicação entre as cargas de uma mesma \textit{microgrid}. Igualmente, uma carga de uma microgrid não tem acesso a informações de outras cargas em outras \textit{microdgrids}. Cada microgrid pode ter vários geradores (inclusive geração distribuída) e, nos casos de falta, negocia com outras \textit{microgrids} de forma a cooperarem entre si e realizarem autorrecuperação. Nessa abordagem, os autores não utilizam diretamente muliagentes, mas são claras as suas características compatíveis com SMA. O caráter autônomo das \textit{microgrids}, a gerência distribuída e a negociação são pontos dessa abordagem que são perfeitamente cobertas por SMA.

Uma abordagem que foge à descentralização é proposta por \citeonline{ferreira}. Os autores fazem uso da técnica de algoritmo genético para reconfigurar as chaves do sistema, modelando para isso o problema de autorrecuperação como uma função de minimização. São utilizados múltiplos pontos de falha para verificar o desempenho do algoritmo. O número de soluções diminui conforme o número de pontos de falha aumenta. Nessa abordagem, o processamento do algoritmo é centralizado e os autores cogitam a adição de alguma outra técnica que eliminem resultados redundantes para melhorar o desempenho do algoritmo.

\citeonline{sharma3} propõem uma abordagem também com sistemas multiagente. Os autores utilizam como modelo uma rede em operação na Tasmânia e Singapura. Sobre esse modelo há um sistema multiagente com diferentes agentes que gerenciam cargas, baterias, geradores distribuídos, chaves e regiões do \textit{smart grid}. Nos casos de falta, os agentes trocam mensagens com o objetivo de formar ilhas. Essas ilhas possuem demandas específicas e cargas com prioridades diferentes. Depois disso os agentes comunicam os agentes regionais de modo a estabelecer de que forma a energia será restabelecida, isto é, quais serão as fontes a suprir as demandas da ilha, quais as operações de chaveamento necessárias e quais as cargas que não poderão ser religadas à rede. O sistema tem melhor desempenho não só quando há geração distribuída, mas também quando é levado em conta o caráter incerto dessas fontes.

Verifica-se que há diversas metodologias envolvidas no campo da autorrecuperação com diferentes técnicas utilizadas (centralizadas, descentralizadas, priorizadas, entre outras). Também é possível notar certa tendência para o uso de geração distribuída entre os \textit{smart grids}, aumentando a complexidade desses sistemas, mas por outro lado,  concretizando rico campo para aplicação de sistemas multiagente.

No contexto de autorrecuperação de \textit{smart grids}, este trabalho propõe um algoritmo de autorrecuperação descentralizado para ser executado por um sistema multiagente.  A contribuição deste trabalho está centrada especialmente na forma descentralizada como os agentes se coordenam para alcançar a autorrecuperação da rede.

% -------------------------------------------------------------
\section{Objetivos}
% -------------------------------------------------------------

% ---
\subsection{Gerais}
% ---

O principal objetivo deste trabalho é modelar uma rede elétrica do tipo \textit{smart grid} e, utilizando simulação computacional, realizar a autorrecuperação dessa rede aplicando um algoritmo aqui proposto, algoritmo este executado por um sistema multiagente. Com a autorrecuperação, objetiva-se reforçar a ideia validada por \citeonline{saraiva} de que sistemas multiagente podem ser utilizados para implementar redes inteligentes (\textit{smart grids}), bem como, a sua autorrecuperação. Como consequência da validação da ideia principal deste trabalho, objetiva-se verificar como o caráter distribuído dos sistemas multiagente influencia no processo de autorrecuperação das \textit{smart grids}.

% ---
\subsection{Específicos}
% ---
\begin{itemize}	
	
	\item Utilizar modelos de sistemas elétricos de distribuição que respeitem restrições nativas de uma rede elétrica real para servir de base às simulações propostas neste trabalho;
	
	\item Modelar e implementar um sistema multiagente que ofereça suporte para executar algoritmos de cálculos relativos aos sistemas elétricos de distribuição e tomada de decisão;
	
	\item Propor um algoritmo que seja capaz de executar a autorrecuperação de \textit{smart grids} levando em conta o caráter distribuído do sistema multiagente;
	
	\item Testar, utilizando-se de simulações por computador, a efetividade do algoritmo a ser executado pelos agentes do sistema e, posteriormente, aplicá-lo aos modelos de rede elétrica escolhidos;
	
	\item Verificar como se dá a autorrecuperação no sistema proposto e considerar seus principais aspectos sob a abordagem de sistemas multiagente;
	
	\item Avaliar trabalhos relacionados que utilizem ou não a técnica de sistemas multiagente de forma que embasem as propostas deste trabalho. Para os que utilizam técnicas diferentes, realizar comparações que permitam verificar as principais diferenças para uma abordagem utilizando multiagentes.
	
\end{itemize}

% -------------------------------------------------------------
\section{Metodologia}
% -------------------------------------------------------------

A ideia primária desta pesquisa se baseia no problema de autorrecuperação de redes elétricas, problema que ainda é um desafio para a área dos Sistemas Elétricos de Potência (SEP) por ser este um sistema dinâmico e complexo \cite{saraiva}. A partir deste ponto, foi necessário realizar pesquisas bibliográficas para consolidar o conhecimento sobre o problema e verificar quais materiais e métodos a comunidade científica utiliza para abordá-lo. Ao longo da pesquisa, notou-se que grande parte dos trabalhos escolhidos utilizam técnicas de inteligência artificial ou computacional (como algoritmos genéticos, lógica \textit{fuzzy}, redes neurais e sistemas multiagente) e matemáticas (como análise combinatória e programação linear) para abordar o problema da autorrecuperação. Assim, foi possível notar que é mais vantajoso utilizar um método de aproximação da solução ótima do que modelar o problema em uma função objetivo e tentar alcançar a solução ótima.

A partir de então, baseada na ideia de \citeonline{saraiva}, de que sistemas multiagente podem ser utilizados para implementar \textit{smart grids}, a pesquisa utilizou como ator principal um sistema multiagente para simular uma rede de distribuição de energia elétrica e executar uma série de passos, de forma que fosse possível implementar a autorrecuperação dessa rede. Foi utilizado também um modelo de sistema elétrico de distribuição baseado no sistema elétrico introduzido por \citeonline{baran-wu} em sua publicação, que apresenta um modelo com 33 nós.

Concluída a etapa de escolha dos métodos de abordagem, foi necessário escolher em seguida uma plataforma para implementação de sistemas multiagente que provesse um ambiente para o desenvolvimento dos agentes do sistema de maneira que não fosse necessário tratar dos detalhes de comunicação e de comportamentos dos agentes. Foi então escolhido para essa tarefa o \citeonline{jade}, um \textit{framework} licenciado sob a licença LGPL \cite{lgpl} e baseado na linguagem de programação Java \cite{oracle}. Após isso, foi produzido um algoritmo de alto nível de abstração para realizar a autorrecuperação do sistema. Esse algoritmo é composto por quatro passos básicos e utilizam uma abordagem descentralizada para o controle de \textit{smart grids} durante o processo de autorrecuperação.

Em seguida, foram produzidos diagramas para os agentes do sistema, modelando os comportamentos de acordo com o algoritmo de autorrecuperação e de forma a prover padrões para a realização das tarefas desse algoritmo. Nesse ponto da pesquisa, também foram definidos padrões para a troca de mensagens no sistema, de modo a favorecer a execução do algoritmo de forma distribuída entre os agentes. Há certa preocupação com o algoritmo nessa parte da pesquisa pois uma abordagem distribuída (como sistemas multiagente) confere maior complexidade aos sistemas que a implementam \cite{tanembaum}. Portanto, modelar os agentes e seus comportamentos no sistema exigiu constante validação dos modelos produzidos com o objetivo de manter o sistema descentralizado. Finalmente, o sistema foi implementado seguindo os modelos definidos.

Terminada a etapa de desenvolvimento, foi elaborado um modelo mais simples de rede elétrica, com 8 nós, para realizar testes iniciais com o objetivo de averiguar os primeiros resultados produzidos pelo sistema. Esse modelo foi produzido especificamente para a fase inicial de testes, portanto não possui qualquer estudo anterior que comprove sua validação para o uso em um sistema real. A pesquisa optou por adotá-lo unicamente para servir como modelo-protótipo, onde fosse possível testar os comportamentos do sistema multiagente e verificar se este estava seguindo corretamente os passos do algoritmo de autorrecuperação.

A seguir, o sistema foi executado sobre o modelo de 33 nós, este por sua vez apresentado e validado por \citeonline{baran-wu} e, por conseguinte, utilizado para também validar esta pesquisa. Os testes foram conduzidos apenas fornecendo estímulos para os agentes e verificando os estados do sistema. Os testes revelaram que a autorrecuperação do sistema foi executada com sucesso, sob as condições de cada rede. Ainda foi possível coletar dados a respeito das características sob as quais se deu o processo de autorrecuperação, permitindo obter conclusões a respeito do principal tema desta pesquisa.

% -------------------------------------------------------------
\section{Estrutura do trabalho}
% -------------------------------------------------------------

O presente trabalho está organizado em sete capítulos, onde o presente capítulo trouxe a introdução, contextualização, os trabalhos relacionados, a relevância e motivação desta pesquisa. Os demais capítulos com seus conteúdos são descritos abaixo.

\begin{itemize}
	\item \textbf{\autoref{definicoes}}: trata do aparato teórico que é necessário para embasar este trabalho. Neste capítulo são definidos os sistemas elétricos e seus componentes, bem como o problema em análise e suas consequências. O capítulo também traz definições de alguns termos comuns ao domínio dos Sistemas Elétricos de Potência.
	\item \textbf{\autoref{sma}}: um capítulo dedicado exclusivamente para definir,  descrever e discutir os aspectos da técnica de sistemas multiagente.
	\item \textbf{\autoref{modelagem}}: descreve a forma como foram modelados os sistemas elétricos utilizados para validação da proposta dessa pesquisa. Este capítulo também apresenta detalhes da modelagem do sistema multiagente implementado, bem como dos processos que ele realiza para alcançar a autorrecuperação dos sistemas. Também é realizada uma breve análise de trabalhos relacionados para fortalecer as abordagens utilizadas nesta pesquisa.
	\item \textbf{\autoref{resultados}}: este capítulo descreve como foram realizadas as simulações do sistema elétrico, sua autorrecuperação e apresenta os resultados obtidos dessas simulações. O capítulo também analisa e discute os principais resultados.
	\item \textbf{\autoref{conclusoes}}: capítulo que resume as ideias e as conclusões apresentadas ao longo do trabalho, além dos trabalhos futuros derivados desta pesquisa.
\end{itemize}

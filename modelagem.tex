% ----------------------------------------------------------
% CAPÍTULO 4 - MODELAGEM DO SISTEMA
% ----------------------------------------------------------

% -------------------------------------------------------------
\chapter{Modelagem do sistema}
\label{modelagem}
% -------------------------------------------------------------

Este capítulo tem como objetivo especificar três importantes elementos deste trabalho: os modelos de rede elétrica utilizados, o sistema multiagente modelado e o algoritmo de autorrecuperação proposto para execução no sistema multiagente (SMA). Em especial, a modelagem do algoritmo de autorrecuperação seguiu a ideia de, utilizando-se de falhas simuladas em pontos arbitrários, dar meios para que o sistema multiagente reconfigure a rede de forma a garantir o restabelecimento da energia às cargas que foram afetadas.

Pontua-se aqui que os modelos de rede elétrica utilizados são de redes elétricas de distribuição, sendo empregados os termos ``rede elétrica'' ou simplesmente ``rede'', por questões de simplificação da escrita. Numa primeira etapa são apresentados os modelos de rede elétrica utilizados no trabalho e suas características, bem como topologia, entre outros. A seção a seguir mostra o SMA modelado e as operações que ele realiza. Por fim, são realizadas as considerações do capítulo.


% -------------------------------------------------------------
\section{Definição e características dos modelos de rede elétrica utilizados}
% -------------------------------------------------------------

Para investigar o problema da autorrecuperação e validar a proposta realizada nesta pesquisa é necessário, num primeiro momento, definir os aspectos da rede elétrica na qual se realizarão as simulações de falta e se executará o algoritmo de autorrecuperação. São utilizados dois modelos de rede elétrica para este fim, sendo a primeira com 8 (oito) nós e a segunda com 33 (trinta e três). O primeiro modelo, com 8 nós, aqui chamado de topologia \textit{8-bus}, foi concebido para este trabalho com o fim de testes primários para o SMA aqui proposto, enquanto que o segundo modelo, o de 33 nós, nomeado topologia \textit{33-bus} é um modelo da IEEE, com dados relatados por \citeonline{baran-wu}, com o fim de validar esta proposta por meio de um modelo já utilizado em outros trabalhos.

Primeiramente, cabe delinear que todo sistema elétrico possui uma série de restrições que precisam ser seguidas para que opere dentro da normalidade. Não fugindo à essa regra, abaixo tem-se definidas as restrições consideradas para essas duas redes elétricas \cite{eu}:

\begin{itemize}
	\item A rede elétrica ativa --- a rede pela qual circula de fato corrente elétrica --- deve ser radial, isto é, não pode apresentar ciclos;
	\item A rede elétrica deve dispor de linhas de reserva (\textit{tie lines}) que servirão como canal alternativo para religamento dos nós em falta.
\end{itemize}

Definidas as restrições, é necessário modelar a topologia das redes escolhidas. Ambas as redes foram modeladas como grafos, onde os vértices representam as cargas, isto é, os nós demandantes de energia elétrica no sistema, e as arestas representam as linhas de distribuição. As arestas do grafo possuem pesos que no sistema são dados por números complexos, representando a resistência elétrica (parte real) e a reatância (parte imaginária). Assim, quanto maior o valor do peso da aresta na parte real, maior deverá ser a perda elétrica naquela linha, considerando uma corrente elétrica constante. As arestas pontilhadas no grafo representam as \textit{tie lines}, que são linhas desligadas, isto é, com a chave elétrica aberta, e por isso não formam ciclo na rede, apesar de o fazerem visualmente na topologia \textit{33-bus}. A rigor, a rede elétrica, então, possui dois grafos sobrepostos, sendo o primeiro composto pelas arestas preenchidas --- representando a rede elétrica ativa, isto é, o fluxo elétrico que atendes às demandas das cargas --- e o segundo composto por este primeiro grafo somando-se à ele as arestas pontilhadas --- representando a topologia total da rede. Portanto, o interesse desse trabalho é justamente encontrar uma configuração do grafo da rede ativa que, diante de uma falta por queda de linha, consiga restabelecer a energia elétrica aos nós afetados considerando todas as linhas da rede, ativas e inativas.

Em ambas as topologias todas as linhas são controladas por chaves elétricas que podem ser abertas ou fechadas em tempo de execução, com vistas a reconfigurar a rede sempre que necessário por meio de comandos remotos. Chaves elétricas então, para esta abordagem, são instrumentos elétricos controlados por comando remoto que podem fechar ou abrir um circuito, permitindo ou não a passagem de corrente elétrica por uma linha condutora, respectivamente. Considera-se que em cada carga nas duas topologias também existem dispositivos com capacidade de processamento e sensores que mantém dados atualizados da própria carga. Uma carga no sistema é todo nó que possui uma demanda de potência e consome energia elétrica e são as entidades que são afetadas nos casos de falha no fornecimento. A ideia é testar o sistema multiagente modelado por este trabalho fazendo-o lidar com diferentes topologias de rede e, por isso, deverá levar em conta as peculiaridades das duas topologias aqui especificadas.

Diante dessas considerações, são apresentados os grafos correspondentes aos modelos de rede utilizados nas Figuras \ref{fig-8bus-topo} e \ref{fig-33bus-topo} para as topologias \textit{8-bus} e \textit{33-bus}, respectivamente.

% Figura com o grafo da topologia 8-bus
\begin{figure}[htb]
	\caption{\label{fig-8bus-topo}Grafo da topologia \textit{8-bus}}
	\begin{center}
		\includegraphics[scale=0.7]{figures/fig-8bus-topo.pdf}
	\end{center}
	\legend{Fonte: O autor}
\end{figure}

% Tabela com os valores de resistência e reatância nas linhas da topologia 8-bus
\begin{table}[htb]
	\IBGEtab{
		\caption{Valores de resistência e reatância nas linhas da topologia \textit{8-bus}}
		\label{tab-8bus-resistencias}
	}{
		\begin{tabular}{ccc}
			\toprule
			Linha	& Resistência ($ \Omega $)	& Reatância ($ \Omega $) \\
			\midrule
			$ \overline{0,1} $	& 0.0922	& 0.0470 \\
			$ \overline{1,3} $	& 0.0669	& 0.0864 \\
			$ \overline{1,4} $	& 0.1811	& 0.1941 \\
			$ \overline{4,6} $	& 1.1966	& 1.1550 \\
			$ \overline{1,2} $	& 0.1500	& 0.1500 \\
			$ \overline{4,5} $	& 0.1500	& 0.1500 \\
			$ \overline{4,7} $	& 0.8190	& 0.7070 \\
			\bottomrule
		\end{tabular}
	}{
		\legend{Fonte: O autor}
	}
\end{table}

A título de informação, as tensões nominais nos nós-fonte 0 (zero), 2 (dois), 5 (cinco) e 7 (sete) da topologia \textit{8-bus} são todas iguais a 12,66 kV, decaindo ao longo da rede. O mesmo vale para a topologia \textit{33-bus}, porém esse valor é aplicado somente ao nó 0 (zero), única fonte no modelo. Em termos práticos, esses nós-fontes são representações das subestações de distribuição da rede elétrica e são localizadas em diferentes regiões geográficas, além de possuírem características peculiares. A \autoref{tab-8bus-resistencias} detalha os valores de resistência e reatância nas linhas de distribuição da topologia \textit{8-bus}, valores que influenciam nos cálculos de perda elétrica no sistema. Os valores de resistência e reatância para a topologia \textit{33-bus} estão definidos no trabalho de \citeonline{baran-wu}, entretanto podem ser consultados no apêndice deste texto.

% Figura com o grafo da topologia 33-bus
\begin{figure}[htb]
	\caption{\label{fig-33bus-topo}Grafo da topologia \textit{33-bus}}
	\begin{center}
		\includegraphics[scale=0.6]{figures/fig-33bus-topo.pdf}
	\end{center}
	\legend{Fonte: O autor}
\end{figure}

A topologia \textit{8-bus} foi concebida para operar com os primeiros testes do SMA aqui proposto, por isso possui uma peculiaridade em relação à topologia \textit{33-bus}. A topologia \textit{8-bus} possui múltiplas fontes, exatamente quatro. Devido ao fato de ser uma topologia pequena, para aumentar o estresse no SMA em relação ao número de possibilidades de configurações que o sistema pode assumir após o processo de autorrecuperação foi necessário criar múltiplas fontes e aumentar a densidade de \textit{tie lines} em relação ao número de linhas do modelo, que nessa topologia é de $3 \over 7$ contra $5 \over 37$ da topologia \textit{33-bus}. A topologia \textit{33-bus} já conta com apenas uma fonte, contudo possui um maior número de nós, tendo como objetivo testar o desempenho do sistema quando se tem muitos nós para gerenciar.

A topologia \textit{8-bus} é um resultado direto dos primeiros testes realizados com o sistema multiagente proposto e por isso não é encontrada em nenhuma literatura. Durante os primeiros testes com o modelo de SMA e o algoritmo de autorrecuperação, foi necessário produzir diversos modelos de redes pequenos para que fosse possível validar as etapas do algoritmo e atestar a coerência das ações tomadas pelos agentes. Dessa forma, foi proposto um modelo com 8 (oito) nós pelo fato deste culminar em um número menor de \textit{logs} e saídas no terminal do sistema operacional, tornando a análise dos resultados mais legível.

Durante a evolução do sistema, percebeu-se a necessidade de verificar o comportamento do sistema para situações onde mais de uma fonte estivesse disponível. Dessa forma, para acompanhar o elevado número de \textit{tie lines} sugeridos para a topologia \textit{8-bus} criou-se quatro pontos de fonte de energia (nós 0, 2, 5 e 7). Isso proporciona ao algoritmo mais de uma possibilidade de religamento da ilha na maioria dos casos.

Por fim, para aproximar essa topologia à um modelo mais real, foram utilizados os dados técnicos do modelo de 33 nós de \citeonline{baran-wu}, tais quais resistência e tensão nos nós, para nortear a escolha dos valores para a topologia \textit{8-bus}. Esses valores foram então escolhidos e estão descritos na \autoref{tab-8bus-resistencias}. Esta pesquisa optou por produzir, formalizar e utilizar esse modelo nas simulações para verificar as diferenças que o sistema pudesse apresentar ao operar sobre topologias com muitos ou poucos nós.


% -------------------------------------------------------------
\section{O sistema multiagente e suas operações}
% -------------------------------------------------------------

O sistema multiagente produzido por esta pesquisa foi projetado com o fim de se adaptar dinamicamente às diferentes topologias de redes que possam ser estudadas com o fim de validá-lo. Tal sistema possui dois tipos de agentes com diferentes atribuições: os \textbf{agentes de carga} (LA) e os \textbf{agentes de chave} (SA). Agentes de carga são as principais personagens do SMA, pois são eles que executam a maior parte das operações no sistema. Cada LA do sistema monitora e reside em apenas uma carga da rede, ou seja, em termos práticos, um LA está relacionado apenas a uma carga, de modo que uma carga no sistema poder ser referida como um LA indiscriminadamente. Abaixo seguem as mais importantes atribuições desses agentes:

\begin{itemize}
	\item Comunicar-se com LA vizinhos em busca de informações dos seus estados;
	\item Manter atualizados dados como alimentação, demanda, tensão e outros relativos à carga onde reside, de maneira a detectar alterações no ambiente e estar pronto a enviar atualizações do seu estado a quem as requisitar;
	\item Conectar-se a novos LA, em tempo de execução, para moldar-se ao dinamismo do sistema;
	\item Eleger um agente ativo nos casos de falta -- agentes ativos serão discutidos mais à frente;
	\item No caso de ser eleito como agente ativo, realizar operações de mapeamento de rede e coordenar os agentes afetados pela falta.
\end{itemize}

Agentes de chave são marjoritariamente passivos no sistema e suas operações são bem mais simples que as dos LA. Os SA residem em dispositivos nas linhas do sistema elétrico e guardam informações estáticas --- como resistência e reatância --- e dinâmicas --- como estado da chave --- da linha. Em semelhança à relação dos LA com as cargas, cada SA está relacionado a uma e somente uma linha do sistema elétrico. Abaixo tem-se a lista suas principais atribuições dos SA:

\begin{itemize}
	\item Ser o mediador entre a conexão de dois LA, em tempo de execução;
	\item Receber comandos de abertura e fechamento de chaves, executá-los e notificar os LA próximos sobre o ocorrido.
\end{itemize}

Durante o processo de autorrecuperação, agentes de carga podem assumir dois papéis distintos no sistema: o de agente de carga ativo (AA) e o de agente de carga passivo (PA). AA é um LA eleito dentre o conjunto dos nós que sofreu falta para ser a autoridade que representa esse conjunto e que toma as decisões por ele. PA são os agentes que elegem o AA e aguardam por instruções a respeito de como devem cooperar para reconfigurar o sistema. A forma como esses agentes se coordenam para reconfigurar a rede e restaurar a energia na área em falta segue o algoritmo descrito na \autoref{algo-autorec}.

Uma outra característica do SMA proposto é a minimização do conhecimento da topologia da rede pelos agentes. Em termos de sistema, isso significa que cada LA tem conhecimento e se comunica apenas com os LA vizinhos que estão diretamente conectados a ele. Portanto, a topologia da comunicação entre os agentes do sistema, quando não há perturbações na rede, é equivalente à topologia geral do sistema, incluindo linhas ativas e \textit{tie lines}. A \autoref{fig-topo-comm} apresenta os grafos com as topologias de comunicação dos sistemas \textit{8-bus} e \textit{33-bus}, onde os vértices representam os agentes de carga e as arestas pontilhadas representam os caminhos de comunicação entre esses agentes.

% Figura com os grafos das topologias de rede de comunicação nos sistemas 8-bus e 33-bus
\begin{figure}[htb]
	\caption{\label{fig-topo-comm}Grafos das topologias de rede de comunicação nos sistemas \textit{8-bus} \textit{33-bus}}
	\begin{center}
		\includegraphics[scale=0.45]{figures/fig-topo-comm.pdf}
	\end{center}
	\legend{Fonte: O autor}
\end{figure}

Em suma, as estruturas de comunicação representadas na \autoref{fig-topo-comm} indicam que, na topologia \textit{33-bus} por exemplo, o LA que reside no nó 1 (simplesmente LA1) só conhece e se comunica com LA0, LA2 e LA18, ao passo que o LA8 se comunica com LA7, LA9 e LA14, mesmo que a linha $\overline{8,14}$ seja uma \textit{tie line}. Essa perspectiva de comunicação ajuda a manter o sistema distribuído, limitando as despesas computacionais apenas aos nós que estão diretamente envolvidos nos eventos que ocorrem na rede. Vale ressaltar que o SMA aqui descrito é do tipo reativo, significando que realiza suas funções somente em decorrência dos estímulos ocorridos na rede.

% -------------------------------------------------------------
\subsection{Algoritmo de autorrecuperação}
\label{algo-autorec}
% -------------------------------------------------------------

Para alcançar a autorrecuperação os agentes do sistema precisam seguir uma série definida de passos para estabelecer prioridades e resolver dependências e conflitos entre eles. Considera-se que, ao ocorrer a falha em uma das linhas do sistema, será naturalmente formado um conjunto de nós que sofreram interrupção no fornecimento de energia elétrica. A este conjunto chama-se \textit{ilha}. A seguir, são apresentados os passos do algoritmo, seguidos de uma breve descrição a respeito das suas principais atividades. As próximas subseções descrevem detalhadamente os processos que ocorrem no sistema durante a execução de cada um destes passos.

\begin{itemize}
	\item \textbf{Passo 1.} \textit{Isolamento da área em falta:} esse primeiro passo consiste basicamente em trocar mensagens entre os agentes adjacentes para identificar quais estão sem o suprimento de energia. Todos os LA que detectarem a falta realizam esse procedimento para que se conheça a área do sistema que foi afetada.
	\item \textbf{Passo 2.} \textit{Eleição do agente ativo:} depois de isolada a área afetada, é necessário escolher um representante para executar cálculos que permita tomar decisões a respeito da recuperação da ilha. À este representante, dá-se o nome de \textit{agente ativo} (AA). A eleição do AA é feita escolhendo o agente externo com maior ID\footnote{ID é um número inteiro que identifica a carga. É o mesmo usado para identificar um agente de carga no sistema.} da ilha. Agente externo é todo LA que compõe a ilha e tem pelo menos um de seus vizinhos com o fornecimento de energia não interrompido. Essa eleição é realizada através da candidatura dos LA externos por estruturas de dados aqui chamados de \textit{tickets}.
	\item \textbf{Passo 3.} \textit{Mapeamento da ilha:} consiste no processo em que os  agentes informam seus estados e endereços dos SA controladores de linhas próximas a eles para o AA, de forma que este tenha conhecimento de quais são as configurações possíveis que o sistema pode assumir depois da autorrecuperação. Esse processo é realizado por uma troca recursiva de mensagens entre os LA componentes da ilha.
	\item \textbf{Passo 4.} \textit{Tomada de decisão e religamento:} realizando o mapeamento da ilha o AA tem a capacidade de reunir as informações necessárias para executar cálculos e efetuar a tomada de decisão, isto é, escolher de que forma religará a ilha novamente à rede ativa. O AA então requisita as operações de chaveamento necessárias e, finalmente, a energia é restabelecida à ilha.
\end{itemize}

% -------------------------------------------------------------
\subsection{Isolamento}
% -------------------------------------------------------------

% Figura com esquema de comunicação na topologia 8-bus para o LA1 durante o processo de isolamento do algoritmo de autorrecuperação
\begin{figure}[htb]
	\caption{\label{fig-algo-step1}Esquema de comunicação na topologia \textit{8-bus} para o LA1 durante o processo de isolamento do algoritmo de autorrecuperação}
	\begin{center}
		\includegraphics[scale=0.8]{figures/fig-algo-step1.pdf}
	\end{center}
	\legend{Fonte: O autor}
\end{figure}

No primeiro passo do algoritmo ocorrem trocas de mensagens entre os agentes de carga afetados. Ao ser detectada a falta, através dos seus sensores, um LA muda o seu estado disparando o início do algoritmo de autorrecuperação para esse agente. Nesse momento, o agente precisa isolar a área em falta, descobrindo quais agentes ao seu redor podem oferecer suporte para recuperação. Assim, esse LA envia mensagens aos seus vizinhos requisitando que enviem atualizações sobre o seu estado atual. A \autoref{fig-algo-step1} ilustra essa troca de mensagens para o agente de carga 1, na topologia \textit{8-bus}. Analogamente, todos os agentes que detectarem falha realizarão o mesmo processo, de modo que, ao final da troca de mensagens, todos os agentes afetados pela falta terão conhecimento de quais dos seus vizinhos estão na mesma situação e quais não.

% -------------------------------------------------------------
\subsection{Eleição do agente ativo}
% -------------------------------------------------------------

Após a área ser isolada, é hora de escolher o agente ativo da ilha. Para realizar esse processo, todos os LA que forem agentes externos precisarão se candidatar enviando \textit{tickets} contendo o seu ID para um ponto centralizador na rede, de modo que seja possível que dois ou mais agentes não adjacentes consigam interagir indiretamente. Esse ponto centralizador, por conveniência, é uma funcionalidade do agente \textit{Directory Facilitator}, da plataforma JADE, que aceita registros de agentes para publicação de serviços através de uma memória compartilhada, chamada de \textit{página amarela}. Somente agentes da uma mesma plataforma têm acesso a essa memória compartilhada.

Após o registro, um LA candidato deve aguardar o tempo de 370 ms para que todos os LA externos também se candidatem. Ao fim desse tempo, os agentes candidatos realizam a leitura da memória compartilhada, verificam quais agentes se registraram e verificam seus ID. No fim, o agente que tiver o ID igual ao maior ID registrado na página amarela será o agente ativo da ilha, enquanto que os outros agentes candidatos, assim como todos os outros afetados pela falha, automaticamente ficam aguardando comandos do AA para que se inicie o próximo passo do algoritmo de autorrecuperação --- isto é, tornam-se \textit{agentes passivos}.

Destaca-se que se não houver agentes candidatos nessa etapa, todos os agentes se tornam agentes passivos e não realizarão o processo de autorrecuperação. Esse fato acontece quando não há \textit{tie lines} disponíveis para a autorrecuperação. No exemplo da \autoref{fig-algo-step2}, o agente LA4 será o AA da ilha por ter ID maior que os outros candidatos.

% Figura com esquema de candidatura dos LA 1 e 4 na topologia 8-bus durante o processo de eleição do algoritmo de autorrecuperação
\begin{figure}[htb]
	\caption{\label{fig-algo-step2}Esquema de candidatura dos LA 1 e 4 na topologia \textit{8-bus} durante o processo de eleição do algoritmo de autorrecuperação}
	\begin{center}
		\includegraphics[scale=0.9]{figures/fig-algo-step2.pdf}
	\end{center}
	\legend{Fonte: O autor}
\end{figure}

% Figura com esquema de comunicação na topologia \textit{8-bus} durante o processo de mapeamento da ilha por troca de mensagens recursivas
%\begin{figure}[htb]
%	\caption{\label{fig-algo-step3}Esquema de comunicação na topologia \textit{8-bus} durante o processo de mapeamento da ilha por troca de mensagens recursivas}
%	\begin{center}
%		\includegraphics[scale=0.7]{figures/fig-algo-step3b.pdf}
%	\end{center}
%	\legend{Fonte: O autor}
%\end{figure}

% -------------------------------------------------------------
\subsection{Mapeamento da ilha}
% -------------------------------------------------------------

Nesse ponto do algoritmo, o AA precisa conhecer todos os agentes, linhas e \textit{tie lines} que compõem a área afetada para que ele possa tomar decisão sobre qual configuração de rede será utilizada na autorrecuperação. Inicia-se então o processo de mapeamento de ilha entre os agentes afetados.

O mapeamento de ilha se baseia no algoritmo de busca em profundidade de grafos, sendo portanto um algoritmo de busca recursiva. O processo de mapeamento por troca de mensagens se inicia no AA da ilha, o qual solicita aos seus vizinhos em falta que retornem para ele uma lista com informações sobre os vizinhos destes que também estão em falta, além das informações das linhas entre eles. Nessa lista também devem conter informações sobre a linha que falhou e sobre as \textit{tie lines} disponíveis. À essa lista chama-se sub-mapa de rede, que é a visão individual que cada agente da ilha possui sobre o ambiente.

Os agentes adjacentes ao AA que também têm vizinhos afetados pela falta, por sua vez, farão requisições a estes solicitando os seus sub-mapas. Após ter recebido os sub-mapas de todos os seus vizinhos, tal agente soma à lista as informações da sua leitura do ambiente e retorna-a ao agente que a solicitou. Esse processo é repetido recursivamente até que todos os nós afetados enviem seus sub-mapas.

Para ilustrar, a \autoref{fig-algo-step3} apresenta um esquema de como se dá a troca de mensagens no sistema durante o processo de mapeamento da ilha. O esquema da figura evidencia o estado dos agentes e as mensagens que eles trocam ao longo do tempo, considerando que todos os agentes trocam mensagens ao mesmo tempo. Não são levados em conta os \textit{delays} de transmissão ou processamento.
%Como adicional, o \autoref{algorithm} resume, em pseudocódigo, como é realizado o mapeamento na visão de um agente passivo.

% Figura com esquema de comunicação na topologia \textit{8-bus} durante o processo de mapeamento da ilha por troca de mensagens recursivas
\begin{figure}[htb]
	\caption{\label{fig-algo-step3}Esquema de comunicação na topologia \textit{8-bus} durante o processo de mapeamento da ilha por troca de mensagens recursivas}
	\begin{center}
		\includegraphics[scale=0.7]{figures/fig-algo-step3.pdf}
	\end{center}
	\legend{Fonte: O autor}
\end{figure}

% ---------------------
% Aqui vai o algoritmo do mapeamento.
% ---------------------

% -------------------------------------------------------------
\subsection{Tomada de decisão e religamento}
% -------------------------------------------------------------

Após o término do mapeamento da ilha pelos agentes afetados, o AA possui todas as informações das quais necessita para realizar a tomada de decisão. Esse conjunto de informações é denominado \textit{mapa de ilha} e incluem os estados dos agentes afetados pela falta, além do estado das linhas ligadas à esses agentes, o que permite ao AA determinar onde ocorreu a falha e quais \textit{tie lines} pode usar.

O processo de tomada de decisão é realizado através do método de varredura de \citeonline{saraiva}, que retorna informações como as tensões nos nós e as perdas elétricas de um sistema elétrico, desde que informados o nó por onde inicia o fluxo e as estruturas de dados que representam o grafo da rede elétrica. Esse é um método determinístico e considera especialmente as resistências nas linhas, a tensão nominal da fonte e as demandas das cargas naquele momento.

% Figura com esquema da tomada de decisão por um agente ativo da ilha utilizando o método de varredura de \citeonline{saraiva}}
\begin{figure}[htb]
	\caption{\label{fig-algo-step4}Esquema da tomada de decisão por um agente ativo da ilha utilizando o método de varredura de \citeonline{saraiva}}
	\begin{center}
		\includegraphics[scale=0.7]{figures/fig-algo-step4.pdf}
	\end{center}
	\legend{Fonte: O autor}
\end{figure}

O algoritmo de varredura deverá ser executado tantas vezes quanto há \textit{tie lines} disponíveis para religamento, já que cada \textit{tie line} representa uma configuração de rede diferente. Entende-se como \textit{configuração de rede} o conjunto de linhas ativas (com chave elétrica fechada) por onde flui a corrente elétrica que supre a demanda das cargas. Cada configuração de rede é modelada como uma árvore, cuja raiz é o nó de onde parte o fluxo de energia. A \autoref{fig-algo-step4} apresenta como são consideradas pelo AA as configurações de rede possíveis do sistema.

Destaca-se que o AA sempre escolhe a configuração de rede que render menor valor de perdas elétricas, escolha que é interessante para a rede, já que terá um aproveitamento melhor da corrente elétrica que flui através dela. Após escolher qual configuração será utilizada pelo sistema, o AA requisita primeiramente a abertura de chave ao SA que mantém controle da linha que falhou (para evitar futuros ciclos na rede) e, após confirmação, requisita ao SA da \textit{tie line} escolhida o fechamento da chave, restaurando o fluxo de cargas na área afetada.

% -------------------------------------------------------------
\subsection{Diagramas}
% -------------------------------------------------------------

Em suma, as Figuras \ref{fig-diag-seq-geral} e \ref{fig-diag-ativ-algo} apresentam, respectivamente, um diagrama de sequência UML e um diagrama de atividades UML a respeito algoritmo executado pelo SMA proposto. O diagrama da \autoref{fig-diag-seq-geral} retrata de modo geral como se dão as trocas de mensagens ao longo do algoritmo de autorrecuperação.

% Figura com diagrama de sequência do algoritmo de autorrecuperação
\begin{figure}[htb]
	\caption{\label{fig-diag-seq-geral}Diagrama de sequência do algoritmo de autorrecuperação}
	\begin{center}
		\includegraphics[scale=0.9]{figures/fig-diag-seq-geral.pdf}
	\end{center}
	\legend{Fonte: O autor}
\end{figure}

Analisando agora o diagrama da \autoref{fig-diag-ativ-algo} torna-se importante dissertar sobre os dois limiares de tempo empregados nessa abordagem. O limiar na primeira atividade do diagrama diz respeito ao intervalo de tempo que o agente deverá esperar para realizar nova leitura dos sensores para atestar se há ou não fornecimento de energia.

Esse limiar foi escolhido com base em tentativa e erro, levando em conta que um limiar muito alto pode sacrificar o desempenho do algoritmo de autorrecuperação, já que cada LA demoraria para identificar a falta. Por outro lado, um limiar muito pequeno pode significar um gasto desnecessário de recursos computacionais, recursos esses que são escassos na maioria dos sistemas embarcados. Assim, este primeiro limiar foi ajustado para o valor de 200 ms, julgando esta pesquisa ser um valor compatível com as expectativas do sistema.

% Figura com diagrama de atividades do algoritmo de autorrecuperação
\begin{figure}[htb]
	\caption{\label{fig-diag-ativ-algo}Diagrama de atividades do algoritmo de autorrecuperação}
	\begin{center}
		\includegraphics[scale=0.7]{figures/fig-diag-ativ-algo.pdf}
	\end{center}
	\legend{Fonte: O autor}
\end{figure}

O outro limiar, na transição entre as atividades 4 e 5 do diagrama de atividades, diz respeito ao tempo de espera pela candidatura de agentes ativos do sistema. Esse é o tempo que os LA da ilha devem aguardar para conferir os candidatos do sistema e eleger o AA. Isso é necessário pelo fato de os agentes não terem conhecimento de agentes não conectados diretamente a eles e, por isso, precisam que absolutamente todos os LA externos se candidatem dentro desse tempo. Ao contrário do limiar de espera pela leitura dos sensores, é interessante diminuir esse limiar ao máximo, pois penaliza diretamente o tempo de autorrecuperação. Porém, o processo candidatura dos agentes externos é muito dependente da arquitetura de hardware adotada em cada sistema, o que torna difícil definir um limiar mínimo. Além disso, esse limiar deve ser maior que o limiar para releitura dos sensores dos agentes para garantir que, no pior dos casos, todos os agentes externos tenham tido tempo de verificar que sofreram falta e se candidatar.

O tempo escolhido foi de 370 ms, valor escolhido por tentativa e erro por nunca ser extrapolado durante os testes realizados. Esse limiar significa que o tempo de autorrecuperação do sistema jamais será menor que 370 ms, pois necessariamente os AA deverão esperar pelo fim desse tempo para então se elegerem.


% -------------------------------------------------------------
\section{Considerações do capítulo}
% -------------------------------------------------------------

Neste capítulo foi possível revisar alguns trabalhos que empregam diferentes técnicas para abordar autorrecuperação em \textit{smart grids}. Sistemas multiagente são muito utilizados na abordagem de problemas dos \textit{smart grids} devido ao seu caráter distribuído, atributo nativo dessa técnica. Foram descritos os detalhes técnicos dos modelos de rede elétrica utilizados para validar o SMA proposto, um de 8 nós e outro de 33 nós. Além disso, descreveu-se o SMA com seus agentes principais: agente de carga e agente de chave. Os LA podem assumir diferentes papéis no sistema durante o algoritmo de autorrecuperação. O próximo capítulo traz detalhes das simulações realizadas e validam os resultados esperados dos sistemas propostos no presente capítulo.
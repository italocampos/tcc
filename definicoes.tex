% ----------------------------------------------------------
% CAPÍTULO 2 - SISTEMAS ELÉTRICOS E AUTORRECUPERAÇÃO
% ----------------------------------------------------------

% -------------------------------------------------------------
\chapter{Sistemas elétricos e autorrecuperação}
\label{definicoes}
% -------------------------------------------------------------

Este capítulo trata das definições teóricas nas quais se baseiam as técnicas utilizadas nesta pesquisa e o seu problema de investigação. Primeiramente, adentra-se no mais fundamental conceito onde se baseia toda a cadeia do estudo realizado por este trabalho, os Sistemas Elétricos de Potência (SEP). Após isso, serão apresentadas as modalidades de redes elétricas \textit{smart grids} e em seguida o problema de autorrecuperação, centro de interesse desta pesquisa. Ao fim são realizadas considerações sobre o capítulo.


% -------------------------------------------------------------
\section{Sistemas Elétricos de Potência}
\label{sep}
% -------------------------------------------------------------

Sistemas Elétricos de Potência são sistemas complexos especializados na produção, transmissão e fornecimento de energia de forma a suprir uma demanda variável ao longo do tempo. Nas palavras de \citeonline[p.2]{blume}, Sistemas Elétricos de Potência --- ou simplesmente SEP --- são \textit{``sistemas de fornecimento de energia elétrica em tempo real''}. Isso significa que a energia é produzida, transmitida e fornecida ao requisitante no momento em que este a demanda \cite{blume}.

Basicamente, a humanidade aproveita energia nas mais diversas formas, mas a energia elétrica se tornou a principal fonte movedora das atividades humanas, especialmente por possuir características que favorecem seu transporte a diversos lugares. Porém, essa forma de energia não é comum na natureza, pelo menos não em quantidades suficientes para suprir as demandas humanas. Assim, se torna necessário utilizar meios de transformar energia das mais diversas formas e fontes em energia elétrica.

Às formas de energia que o SEP transforma em energia elétrica chama-se \textit{energia primária} e ao produto final dos SEP, a energia elétrica, chama-se então \textit{energia secundária} \cite{gebran}. O Sistema Elétrico de Potência é comumente especializado em três subsistemas, compreendidos em subsistema de \textbf{geração}, de \textbf{transmissão} e de \textbf{distribuição} --- também chamado de sistema de subtransmissão --- de energia elétrica \cite{blume}. Esquematicamente, pode-se descrever o SEP como na \autoref{fig-sep-schema}. As setas em vermelho evidenciam as trocas de contexto da energia gerada ao longo dos subsistemas do SEP; já as setas em cinza estabelecem a ordem de passagem da energia entre uma seção e outra de cada subsistema.

% Figura com modelo esquemático da organização dos subsistemas do SEP
\begin{figure}[htb]
	\caption{\label{fig-sep-schema}Modelo esquemático da organização dos subsistemas do SEP}
	\begin{center}
		\includegraphics[scale=0.7]{figures/fig-sep-schema.pdf}
	\end{center}
	\legend{Fonte: Adaptado de \citeonline[p. 3]{blume}}
\end{figure}

Quando se fala em transmissão e armazenamento de energia, tem-se um grande dilema em relação aos SEP: não se consegue armazenar quantidade suficiente de energia secundária para alimentar grandes demandas, porém é mais fácil transportá-la. Por outro lado, é mais viável, armazenar grandes quantidades de energia primária, tecnologicamente falando, ao passo que seu transporte até o consumidor final é inviável \cite{gebran}. Para ilustrar a problemática, pode-se imaginar que é mais fácil transportar energia elétrica, porém não se dispõe de tecnologia para armazenar um volume de energia suficiente para iluminar uma cidade, ao passo que é mais fácil represar as águas de um rio, porém inviável transpô-lo para dentro de uma cidade com o fim de se aproveitar a energia cinética de suas águas. Uma vez que o SEP está sujeito à essas condições, os consumidores precisam utilizar toda a energia que é gerada, sob pena de desperdiçar o excedente \cite{gebran}. Assim, é justificável que os SEP tenham as especializações descritas no esquema da \autoref{fig-sep-schema}, de forma que possui características específicas em cada um de seus subsistemas. Esses subsistemas precisam responder com eficiência à complexidade que possuem e às demandas as quais são expostos, sendo necessário controlá-los em tempo real \cite{blume}.

Durante todo o processo de geração, transmissão e distribuição de energia pelo SEP, a energia elétrica é transportada à diversas tensões elétricas, de acordo com as características dos locais por onde ela passa. A \autoref{tab-nivel-tensoes} apresenta os valores que classificam os níveis de tensões padrões nos SEP e a \autoref{tab-tensoes-sep} mostra em quais níveis de tensão operam cada subsistema do SEP. Esses níveis de tensão se justificam principalmente pela distância que a energia elétrica percorre em cada processo e pela relação com as perdas elétricas.

% Tabela com as classificação dos níveis de tensão
\begin{table}[htb]
	\IBGEtab{%
		\caption{Classificação dos níveis de tensão}%
		\label{tab-nivel-tensoes}
	}{%
		\begin{tabular}{cc}
			\toprule
			Classificação		& Nível de tensão \\
			\midrule
			Baixa tensão		& até 1 kV \\
			Média tensão		& de 1 a 66 kV \\
			Alta tensão			& de 69 kV a 230 kV \\
			Extra-alta tensão	& de 230 kV a 800 kV \\
			Ultra-alta tensão	& maior que 800 kV \\
			\bottomrule
		\end{tabular}%
	}{%
		\legend{Fonte: Retirado de \citeonline{gebran}}%
	}
\end{table}

% Tabela com os níveis de tensão por subsistema do SEP
\begin{table}[htb]
	\IBGEtab{
		\caption{Níveis de tensão por subsistema do SEP}
		\label{tab-tensoes-sep}
	}{
		\begin{tabular}{cc}
			\toprule
			Subsistema		& Nível de tensão \\
			\midrule
			Geração			& Baixa e média tensão\\
			Transmissão		& Alta, extra-alta e ultra-alta tensão \\
			Distribuição	& Baixa e média tensão \\
			\bottomrule
		\end{tabular}
	}{
		\legend{Fonte: Baseada no trabalho de \citeonline{gebran}}
	}
\end{table}


A energia elétrica é a energia aproveitada do movimento dos elétrons livres das partículas dos materiais. Esse movimento é chamado de corrente elétrica e atualmente pode ser produzida na forma de corrente contínua ou corrente alternada \cite{halliday}. É possível que um SEP trabalhe com essas duas formas de corrente, porém é quase padrão encontrar SEP que trabalhem com energia de corrente alternada devido suas características de transmissão, que são mais econômicas que o transporte por corrente contínua \cite{rauf}.

Considerando essas características, um SEP tem o objetivo principal de fornecer energia aos consumidores que estão, geralmente, localizados à longas distâncias dos centros de geração de energia elétrica. Ainda assim, segundo \citeonline{gebran} há três subobjetivos que um SEP precisa levar em consideração durante sua operação. São eles:

\begin{enumerate}
	\item \textit{Continuidade}: fornecer e manter o fornecimento de energia elétrica aos consumidores levando em consideração a demanda variável, isto é, gerando energia à medida que há necessidade;
	\item \textit{Qualidade}: fornecer energia elétrica dentro dos padrões de qualidade estabelecidos pelos órgãos reguladores com um mínimo possível de variação;
	\item \textit{Eficiência}: fornecer energia elétrica de maneira que haja o menor custo e desperdício o quanto possível.
\end{enumerate}

\citeonline{gebran} salienta que esses objetivos seguem uma ordem concisa, na qual não é possível descender ao nível subjacente sem ter alcançado o objetivo anterior. Isso significa que, por exemplo, não é possível manter o fornecimento de energia dentro dos padrões de qualidade sem entregar um serviço contínuo.

Passa-se agora a discorrer as principais características dos subsistemas de um Sistema Elétrico de Potência.

% -------------------------------------------------------------
\subsection{Sistemas de geração}
\label{geracao}
% -------------------------------------------------------------

O sistema de geração é a seção do SEP responsável por transformar energia primária em energia elétrica (secundária). Esse processo atualmente é realizado de várias maneiras ao redor do mundo, utilizando diversos tipos de energia primária, como energia cinética hidráulica, solar, eólica, térmica, nuclear, geotérmica, maremotriz e combustíveis fósseis \cite{gebran}. Cada região do globo possui características sazonais diferentes, o que permite aos países explorarem menos ou mais determinados tipos de energia primária. Ainda assim, entre os maiores polos produtores de energia elétrica, destacam-se as hidrelétricas e os combustíveis fósseis. Existem grades discussões de cunho social e ambiental a respeito da exploração desses tipos de energia devido aos grandes impactos que implicam na adoção de usinas que explorem rios e na emissão de gases maléficos à saúde e ao meio ambiente. Por esse motivo há um crescente incentivo pelo uso das chamadas \textit{energias limpas} --- compreendem principalmente em energia eólica e solar ---, pois têm um impacto ambiental muito menor se comparadas às principais formas de exploração de energia primária. Embora a energia limpa seja uma alternativa viável de produção de energia elétrica, sua exploração possui a característica de ser incerta, pois depende de fatores climáticos e de não conseguir armazenar a energia em forma primária, sendo a produção de energia elétrica limitada à disponibilidade da energia primária.

A energia que é produzida nos SEP utilizando energia cinética --- como as hidrelétricas e as usinas eólicas --- segue um esquema de geração que é apresentado na obra e \cite{gebran}. Há três passos básicos nesse esquema, onde o primeiro é o responsável por produção de energia mecânica através da ativação de uma turbina pelo fluxo de entrada (da energia primária), o segundo é a produção de energia potencial elétrica por um gerador e, finalmente, a obtenção da corrente elétrica, que pode ser contínua ou alternada. A \autoref{fig-generation-scheme} retrata esse esquema.

% Figura com modelo esquemático de geração de energia
\begin{figure}[htb]
	\caption{\label{fig-generation-scheme}Modelo esquemático de geração de energia}
	\begin{center}
		\includegraphics[scale=0.7]{figures/fig-generation-scheme.pdf}
	\end{center}
	\legend{Fonte: Adaptado de \citeonline[p. 13]{gebran}}
\end{figure}

A energia na forma elétrica é a melhor em termos de transporte e controle \cite{freitas}, porém exige um controle complexo de forma que se evite o colapso da rede através de sobrecarga, curto-circuito ou corte. É desejável que se empreguem tensões na geração de energia que permitam o atendimento à demanda de energia elétrica de maneira satisfatória, entretanto o processo de geração é feito em voltagens de baixa e média tensão \cite{gebran}, conforme mostrado na \autoref{tab-tensoes-sep}, porque há uma limitação no fator de isolamento dessas cargas, que deixa de ser eficaz quando uma linha, por exemplo, é submetida a uma tensão muito alta \cite{sarma}.

Um grande trunfo usado para contornar o problema de transmissão de cargas elétricas geradas à altas tensões é o uso de corrente elétrica alternada, tipo padrão de corrente gerada nas usinas hidrelétricas e uma das formas mais usadas de produzir energia elétrica. A nível nacional, a \citeonline{epe-anuario} divulgou no Anuário Estatístico de Energia Elétrica de 2018 que as hidrelétricas correspondem à produção de 60,2\% da capacidade instalada de geração elétrica no Brasil, assumindo ainda o topo da matriz energética do país. O gráfico na \autoref{graf-brazil-energy-matrix} resume a composição da matriz energética elétrica do Brasil.

% Figura com gráfico da matriz energética do Brasil
\begin{figure}[htb]
	\caption{\label{graf-brazil-energy-matrix}Matriz energética do Brasil em 2017}
	\begin{center}
		\includegraphics[scale=1.0]{figures/graph-brazil-energy-matrix.png}
	\end{center}
	\legend{Fonte: Anuário Estatístico de Energia Elétrica 2018 \cite{epe-anuario}}
\end{figure}

Depois de gerada, a energia precisa percorrer grandes distâncias até alcançar o seu destino final. Esse é o principal objetivo dos sistemas de transmissão, descritos a seguir.

% -------------------------------------------------------------
\subsection{Sistemas de transmissão}
\label{transmissao}
% -------------------------------------------------------------

O sistema de transmissão é a parte do SEP responsável por transmitir a energia elétrica do sistema de geração às subestações de distribuição do SEP, componentes primais do sistema de distribuição. O processo de transmissão é realizado através de grandes torres interligadas por linhas de alta-tensão, que podem ser aéreas ou subterrâneas. Conforme discutido na \autoref{geracao}, isolantes são os grandes limitadores do transporte de energia por altas tensões e esse fato aumenta o custo de produção de materiais que sejam eficientes para esse fim. Por essa razão, sistemas de transmissão via linhas aéreas, em geral, são mais baratos e largamente utilizados pelo mercado elétrico, uma vez que nesse tipo de sistema os condutores não são envolvidos por um isolante sólido, mas o ar é utilizado como um. Por isso é necessário que haja uma análise fina para definir questões como a tensão à qual determinada linha será submetida, quais tipos de torres usar, a distância mínima entre cada linha condutora, o tipo de material condutor, etc.

O sistema de transmissão precisa vencer grandes distâncias para levar energia aos consumidores finais, sendo necessário lidar com alguns fatores da transmissão, especialmente as perdas elétricas. Perdas elétricas se caracterizam pela conversão espontânea de energia elétrica em outras formas de energia (principalmente energia térmica) que não serão aproveitadas ao final do sistema. Fisicamente falando, as perdas elétricas se dão especialmente pelo \textit{efeito Joule} e podem ser obtidas pela Lei de Ohm que define a potência dissipada num resistor como sendo:

\begin{equation}\label{ohm-1}
P = RI^2
\end{equation}

Nessa relação, $P$ é a potência perdida, $R$ é a resistência elétrica do material da linha de transmissão e $I$ é a corrente que atravessa a linha de transmissão. Analisando matematicamente, mantendo a resistência constante, a potência dissipada e a corrente são grandezas diretamente proporcionais, o que significa que quanto maior for a corrente na linha de transmissão, maior será a potência perdida \cite{blume}. Dessa forma é necessário se considerar meios que permitam a transmissão de energia elétrica com uma corrente mínima possível, mas que seja capaz de cruzar grandes distâncias. Uma outra consequência da Lei de Ohm descreve relação entre tensão, resistência e corrente elétrica. Essa equação descreve que:

\begin{equation}\label{ohm-2}
P = VI
\end{equation}

Analisando matematicamente nota-se que, para transmitir uma potência $P$ ao consumidor, as grandezas de tensão ($V$) e corrente ($I$) tornam-se inversamente proporcionais, o que significa que aumentando a tensão $V$, a corrente na linha diminui, satisfazendo a restrição imposta pela potência dissipada na linha, descrita na \autoref{ohm-1}.

O tipo de corrente empregada na transmissão de energia elétrica varia de acordo com a finalidade de cada linha de transmissão. Entretanto, é muito comum encontrar sistemas de distribuição que utilizam corrente alternada devido ser um tipo de corrente que exige equipamentos mais simples, menores e baratos que os de corrente contínua, além de conseguirem percorrer maiores distâncias que os transmissores de corrente contínua em transmissões de mesma potência \cite{rauf}. Em outras palavras, sistemas de transmissão de corrente alternada conseguem transmitir a mesma potência que um sistema de transmissão de corrente contínua, porém consumindo menos recursos. No Brasil, o órgão regulamentador determina que as correntes alternadas sejam transmitidas à frequência de 60 Hz, à luz de países como Estados Unidos. Já os países da Europa regulamentam os setores de transmissão de corrente alternada à faixa de 50 Hz \cite{sarma}.

Um outro fator destacável nos sistemas de transmissão é o transporte de energia à tensões muito elevadas, conforme a \autoref{tab-tensoes-sep} descreveu. Teoricamente, a tensão poderia ser elevada a níveis altíssimo, mas nesses casos há problemas com o isolamento das linhas. Embora esse seja um fator limitador, há razões pelas quais são empregadas tensões tão altas quanto possíveis nas linhas de transmissão, conforme observa \citeonline{sarma}:

\begin{itemize}
	\item Aumentar o alcance das linhas de transmissão à maiores distâncias;
	\item Diminuir perdas elétricas (conforme a \autoref{ohm-2});
	\item Diminuir custos com a transmissão.
\end{itemize}

Por fim, os sistemas de transmissão são pontos estratégicos para alimentar consumidores com grande demanda, como indústrias \cite{freitas}. Nesse ponto também é possível conectar geradores distribuídos à rede, especialmente de fontes alternativas, de modo que a rede possua maneiras de suprir a demanda sem depender exclusivamente da geração de uma única usina. Os órgãos mundiais de padronização do setor elétrico incentivam amplamente a agregação de geradores distribuídos de energia limpa aos SEP, embora a agregação distribuída demande grande poder de controle sobre a rede \cite{freitas}.

% -------------------------------------------------------------
\subsection{Sistemas de distribuição}
\label{distribuicao}
% -------------------------------------------------------------

A terceira e última especificação do SEP é o sistema de distribuição, seção que recebe energia em alta tensão, e a distribui à tensão comercial entre os usuários finais, de acordo com a demanda. Conforme a \autoref{tab-tensoes-sep} esse sistema opera à tensões inferiores à 66 kV, sendo mais comuns trabalharem nas faixas inferiores à 35 kV \cite{gebran}. Esses sistemas podem ser monofásicos, bifásicos ou trifásicos. Em termos de bibliografia, como no trabalho de \citeonline{rauf}, considera-se como mais comuns no transporte de energia os sistemas monofásicos e trifásicos, possuindo esses últimos melhor aproveitamento por utilizarem corrente alternada na forma de ondas defasadas para compensar as perdas elétricas durante a transmissão.

Na prática, isso significa um alcance a maiores distâncias para transmissão, consumindo menos recursos, aplicando-se menores condutores em tamanho, peso e quantidade que nos sistemas monofásicos \cite{rauf}. Por essas razões, são adotados com muita frequência linhas trifásicas para a transmissão e distribuição de energia elétrica. Apesar disso, em última milha ainda são utilizadas distribuições monofásicas devido à menor demanda nas residências consumidoras.

No Brasil, é muito mais comum encontrar serviços de distribuição monofásico e bifásico, raramente adotando o sistema trifásico para o setor de distribuição aos consumidores convencionais. A tensão de uso residencial no país é de 110V a 240V, conforme padronização dos órgãos reguladores \cite{freitas}. Como adicional, as concessionárias de energia elétrica são regulamentadas a entregar 5\% da tensão nominal aos consumidores \cite{gebran}.

Os sistemas de distribuição são os setores do SEP mais próximos dos consumidores finais, precisando lidar com a constante transformação de tensão para diversos níveis, dependendo da tensão requerida pelos pontos consumidores de energia \cite{blume}. O controle e instalação de transformadores no sistema de distribuição é realizado pela subestação \cite{freitas}, sendo este um importante ponto de gerência desse sistema. Uma outra característica importante desses sistemas é que eles devem ser radiais. Nas palavras de \citeonline[p.93]{blume}, \textit{``sistema radial significa que somente uma linha terminal do sistema de distribuição é conectado a uma fonte de energia''}. Isso significa que, modelados em um grafo, sistemas de distribuição não podem conter ciclos na malha interna de linhas ativas. Cabe ressaltar que essa é uma das características que norteou o desenvolvimento do projeto apresentado neste trabalho.
%"Radially means that only one end of the distribution power line is connected to a source". \cite{blume}.

Uma subestação de distribuição precisa lidar com o fornecimento de energia elétrica a múltiplos pontos consumidores, fator que requer dessas subestações grande poder de controle das operações que ocorrem no \textit{grid}. Uma falha em alguma das linhas de distribuição, por exemplo, precisa ser detectada, isolada e reparada rapidamente para se consiga manter bons níveis de qualidade de serviço. Porém, o que se vê na prática, à níveis nacionais, são altos índices de demora no atendimento desses casos, já que esse é um serviço majoritariamente manual e depende do contato dos consumidores com a concessionária, informando a falta.

Uma forma de minimizar esse tipo de problema é utilizar equipamentos como transformadores de potência, chaves elétricas de comando remoto, chaves de acionamento manual, filtros, sensores de sobrecarga, medidores, entre outros \cite{freitas}, com o fim de manter a gerência do \textit{grid} em tempo real. Ainda assim os sistemas de distribuição são facilmente passíveis à falhas isoladas e podem representar um grande custo às concessionárias elétricas. Portanto, os sistemas de distribuição são grandemente focados por estudiosos, de modo que encontra-se neles um rico campo para desenvolvimento e aplicação de técnicas que permitam a esses sistemas se tornarem mais robustos e confiáveis. Há uma modalidade de sistema elétrico que é projetada para dar conta de toda essa complexidade, se utilizando de redes de comunicação e tecnologias específicas, as chamadas redes inteligentes, ou ainda \textit{smart grids}.


% -------------------------------------------------------------
\section{Smart Grids}
% -------------------------------------------------------------

No sistema elétrico tradicional, em especial no nível de distribuição, há grandes limitações relacionadas ao controle das operações, pois em geral são realizadas através de mapeamentos por visita em campo, maquinário mecânico e reconfiguração manual. Por ser um sistema com demandas de tempo real, nem sempre é possível realizar manutenções preventivas nas linhas e equipamentos sem desativar ou isolar uma área, tornando esse processo caro e lento. A seção dos SEP mais sensíveis à falha e à maiores variações é o sistema de distribuição, sendo mais suscetível à fatores externos e ambientais \cite{blume}. Esse se torna um grande fator motivador para a aplicação de técnicas que permitam o controle, mapeamento e proteção do sistema em tempo real, bem como torná-lo mais inteligente para realização de operações de maneira automática.

Redes inteligentes ou \textit{smart grids} é o termo usado para se referir a uma modalidade emergente de SEP, que precisa cumprir uma série de objetivos para tornar o seu controle e reconfiguração baseados na informação que ele mesmo produz. \citeonline{saraiva} define \textit{smart grids} como tipos de SEP que utilizam sensores e equipamentos especializados para monitorar o estado da rede e tomar decisões a respeito das manobras necessárias para manter os índices de continuidade, qualidade e eficiência da energia elétrica. Para \citeonline[p.234]{larik}, ``\textit{smart grids é a combinação das tecnologias de informação e comunicação com os sistemas de distribuição e transmissão''}. Em suma, sabe-se que os \textit{smart grids} são uma modalidade de sistemas elétricos que utilizam equipamentos tecnológicos suportados por uma rede de comunicação para alcançar objetivos específicos que melhorem a qualidade do serviço prestado ao consumidor final.

Os \textit{smart grids} precisam manter alguns objetivos para que entreguem um serviço com qualidade ao consumidor final. Não há um consenso entre os autores sobre a quantidade de objetivos a serem atingidos, mas é desejável que os \textit{smart grids} exibam pelo menos algumas funcionalidades. \citeonline{saraiva}, \citeonline{bansal} descrevem as principais:

\begin{itemize}
	\item \textit{Autorrecuperação do sistema:} significa que o sistema precisa se recuperar automaticamente de falhas;
	
	\item \textit{Alta qualidade de energia elétrica fornecida:} significa que a energia entregue ao consumidor final deve ter o mínimo possível de variação de tensão ou outros ruídos;
	
	\item \textit{Resistência a ataques:} a rede deve se proteger de ataques eletrônicos e manter um mapeamento atualizado em tempo real, de forma a permitir que uma influência externa seja detectada no momento em que ocorre;
	
	\item \textit{Suportar a utilização de geradores distribuídos:} o \textit{smart grid} precisa lidar com a geração distribuída de forma a decidir o momento em que novas correntes precisem ser injetadas no \textit{grid}, garantindo a integridade do sistema durante esse processo. Ressalta-se que o \textit{smart grid} é um esforço mundial para o incentivo à produção de energia limpa e renovável, sendo essa a principal modalidade geradora no conjunto de fontes distribuídas nesses sistemas. Essa característica também engloba o uso de baterias ao longo do sistema de distribuição;
	
	\item \textit{Motivar os consumidores a participar ativamente das operações no grid:} significa permitir que o consumidor final tenha acesso a dados em tempo real sobre o consumo e geração de energia, de modo que possibilite que os consumidores tomem decisões sobre economia e consumo de energia. Também se inclui nesse item a visualização dados econômicos a respeito dos mercados do setor elétrico e dos preços de energia, dando ao consumidor a possibilidade de realizar transações financeiras a respeito da energia excedente que ele mesmo produz em geradores particulares. \citeonline{bansal} ressaltam que esse processo só é possível através de uma via de comunicação dupla entre a concessionária e o consumidor;
	
	\item \textit{Controle do grid baseado nos dados de leitura do próprio grid:} significa que o \textit{grid} elétrico deve ter a capacidade realizar leituras periódicas do estado do sistema, alimentar uma base de dados e tomar decisões sobre esses dados de forma que se execute procedimentos antecipados às falhas. \citeonline{saraiva} amplia essa característica à sistemas distribuídos de sensores, de forma que se ganhe em desempenho no sistema e precisão nas medições;
	
	\item \textit{Otimizar a distribuição de energia:} como consequência do maior controle sobre a rede, é possível otimizar a distribuição de energia elétrica de modo a tornar esse processo mais eficiente.
\end{itemize}

Os \textit{smart grids} são sistemas complexos, pois lidam com os mesmos desafios dos sistemas elétricos de potência, como visto ao longo da \autoref{sep}. Dessa maneira, o campos de estudo dos \textit{smart grids} é multidisciplinar \cite{saraiva}, aumentando assim as possibilidades de abordagens possíveis para os diversos problemas das redes inteligentes. Em comparação com os sistemas elétricos tradicionais, os \textit{smart grids} abrem maiores possibilidades em lidar com a geração, transmissão e consumo de energia elétrica, tornando-se um grande atrativo para o mercado energético. A \autoref{sg-vs-tg}, de \citeonline{larik}, resume as principais vantagens de uma rede inteligente sobre os \textit{grids} tradicionais.

% Tabela comparativa entre smart grids e grids tradicionais
\begin{table}[htb]
	\IBGEtab{
		\caption{Comparação entre \textit{grids} tradicionais e \textit{smart grids}}
		\label{sg-vs-tg}
	}{
		\begin{tabular}{c|cc}
			\toprule
			Característica			& Smart grid	& Grid tradicional\\
			\midrule
			Operações				& Digitais		& Eletromecânicas\\
			Comunicação				& Duas vias		& Via única \\
			Geração					& Distribuída	& Centralizada \\
			Controle				& Pervasivo		& Limitado \\
			Qualidade da energia	& Alta			& Moderada \\
			Resistência a falhas	& Adaptável		& Falhas e \textit{blackouts} \\
			Monitoramento			& Automático	& Manual \\
			Recuperação de falhas	& Automático	& Manual \\
			\bottomrule
		\end{tabular}
	}{
		\legend{Fonte: Adaptado de \citeonline{larik}}
	}
\end{table}

Através do seu comportamento autônomo, os \textit{smart grids} são capazes de fornecer meios eficientes e confiáveis de geração e consumo de energia, abrindo o SEP para uma larga escala de aplicações em seus subsistemas \cite{larik}. Assim, os \textit{smart grids} são um grande mercado a ser explorado no futuro, oferecendo ainda meios de se utilizar a energia de forma consciente, racionada e responsável.

% -------------------------------------------------------------
\section{Autorrecuperação de redes elétricas inteligentes}
\label{autorrecuperacao}
% -------------------------------------------------------------

A autorrecuperação de uma rede elétrica representa a execução autônoma de tarefas que, diante de uma falha no fornecimento elétrico, visam o restabelecimento da energia às cargas afetadas do sistema sem a intervenção humana \cite{eu}. Essa é uma funcionalidade importante das redes inteligentes por se integrar ao esforço do ``fornecimento ininterrupto'' de energia ao consumidor \cite{jia}, tornando o sistema elétrico mais confiável no quesito de continuidade e oferecendo um serviço melhor apoiado e com melhor qualidade.

A integração da autorrecuperação em si aos \textit{smart grids} representa uma grande complexidade se levadas em conta a série de informações geradas pelo sistema, as demandas do consumidor e ainda a gerência de múltiplas fontes distribuídas. Em outras palavras, realizar procedimentos de autorrecuperação nos \textit{smart grids} implica no mapeamento e controle das diversas fontes de energia ao longo do \textit{grid}, realizando a tomada de decisão de acordo com a localização e estado das fontes e da rede. Tendo em vista esses objetivos, a autorrecuperação exige que os \textit{smart grids} possuam características que deem suporte à suas operações. \citeonline{jia} descrevem em seu trabalho um sistema de tecnologias dividido em três níveis, que são peças chave para a execução da autorrecuperação em \textit{smart grids}:

\begin{itemize}
	\item \textit{Camada base:} é a camada de mais baixo nível do \textit{smart grid} e engloba equipamentos de \textit{hardware} de telecomunicações e elétrica, como enlace físico de comunicação, roteadores, \textit{switches} inteligentes, terminais de distribuição, equipamentos de proteção e controle, medidores inteligentes, chaves elétricas de controle remoto, etc. É a partir dessa camada que todas as camadas superiores poderão oferecer seus serviços, pois é a camada mais básica e produz os dados que alimentarão a base de conhecimento do sistema, bem como todas as informações referentes ao estado corrente da rede. Em termos práticos, é a cada visível fisicamente que dará suporte às funcionalidades do \textit{smart gris};
	
	\item \textit{Camada de suporte:} resume-se basicamente em toda a infraestrutura lógica da rede de comunicação que deve ser par à rede de distribuição \cite{saraiva}. Essa camada deve se preocupar em oferecer serviços de comunicação de alta velocidade e de via dupla, possibilitando a comunicação em tempo real entre a rede e os consumidores. Aspectos de segurança também devem ser levados em conta, pois é a partir dela que os serviços da rede serão prestados. Compõem essa camada principalmente os protocolos de segurança e rede, além de todo o esquema lógico de comunicação entre os multipontos da rede;
	
	\item \textit{Camada de aplicação:}	é a camada do \textit{smart grid} mais externa, onde serão definidos e publicados os serviços disponibilizados. Entre esses serviços se incluem especialmente os serviços de monitoramento, avaliação, previsão de falhas, controle, recuperação e definição de políticas. Nessa camada vão os algoritmos utilizados para realizar procedimentos na rede (entre eles os de autorrecuperação), a tomada de decisão, a análise dos dados, os algoritmos de previsão, a análise de mercado energético, entre outros recursos.
\end{itemize}

Como adicional, o esquema da \autoref{fig-sg-layers} resume a organização dessa arquitetura de camadas nos \textit{smart grids}. As setas em vermelho indicam o sentido da provisão de serviço entre uma camada e outra e o trânsito das informações geradas pela rede elétrica. Já as setas em cinza representam os comandos dos \textit{stakeholders}, as tomadas de decisão e a realização de operações na rede. Na figura também é possível verificar a dupla via da cadeia de informações que gira em torno de toda a arquitetura dos \textit{smart grids}.

% Figura com esquema de organização em camadas dos smart grids
\begin{figure}[htb]
	\caption{\label{fig-sg-layers}Esquema de organização em camadas dos \textit{smart grids}}
	\begin{center}
		\includegraphics[scale=1.0]{figures/fig-sg-layers.pdf}
	\end{center}
	\legend{Fonte: Esquema do autor com base no trabalho de \citeonline{jia}}
\end{figure}

% -------------------------------------------------------------
\section{Considerações do capítulo}
% -------------------------------------------------------------

Permeando o conceito de redes inteligentes há uma série de pormenores que precisam ser vistos como desafios a serem superados para que os \textit{smart grids} sejam continuamente melhorados. Alguns países, como China e Estados Unidos \cite{saraiva}, já possuem projetos implantados de \textit{smart grids}, mas um grande esforço ainda deve ser feito para que esses sistemas cheguem a países em desenvolvimento, como o Brasil. Embora seja uma área complexa e multidisciplinar, há uma série de pesquisas sendo incentivadas na área, tanto pelo setor público quanto pelo privado, de modo que esses sistemas sejam desenvolvidos e implementados.

Nesse capítulo visou-se definir e contextualizar os SEP e as principais funcionalidades dos \textit{smart grids}, bem como a autorrecuperação e suas consequências. Definidos assim os principais conceitos englobados por esta pesquisa, destaca-se como seu ponto de foco a autorrecuperação em \textit{smart grids}, fazendo uso da técnica de sistemas multiagente para isso. As razões que sustentaram os objetivos desta pesquisa e os trabalhos correlatos ao presente tema estão descritas no capítulo \autoref{sma}.
